﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data;
using System.ComponentModel;

namespace SwiftOrders
{
    public static  class CurrentUser
    {
        public static DAL.User user = new DAL.User();
        public static void LoadPrivilage()
        {
            //Master.MainForm.toolStripDropDownButton1.Text = user.UserName;
          
            foreach (DataRow row in user.GetPrivilaege().Rows)
            {
                if (typeof(CurrentUser).GetField(row[0].ToString()) != null)
                {
                    var i = Convert.ChangeType(row[1], (typeof(CurrentUser).GetField(row[0].ToString()).FieldType));
                    typeof(CurrentUser).GetField(row[0].ToString()).SetValue(new object(), i);
                    
                }
            }
            foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(user))
            {
                Console.WriteLine("{0} = {1}", prop.Name, prop.GetValue(user));
                if (typeof(CurrentUser).GetField(prop.Name) != null)
                {
                    var i = Convert.ChangeType(prop.GetValue(user), (typeof(CurrentUser).GetField(prop.Name).FieldType));
                    typeof(CurrentUser).GetField(prop.Name.ToString()).SetValue(new object(), i);

                }
            }





        }

        #region DataRestrictions
        public static int AccountSecrecyLevel = 1;
        public static int CustomersGroup = 1;
        #endregion
        #region QickAdd
          //if (CurrentUser.AddUOM == false)
          //      { Master.MainForm.SetStatuesBar(LangResorces.ActionnIsNotAllowed, Color.Orange); return; }
        public static bool AddCustomerGroup = true;
        public static bool AddUOM = true;
        public static bool AddItemGroup = true;
        public static bool AddCompany = true;
        public static bool AddActiveIngredint = true;
        public static bool AddVendorGroup = true;
        public static bool AddCustomer = true;
        public static bool AddDrawer = true;
        public static bool AddStore = true;
        public static bool AddItem = true;







        #endregion 
        #region FormAccess
        #region frm_Main
        public static bool NavBarGroup_Data      = true;
            public static bool NavBarGroup_Munf      = true;
            public static bool NavBarGroup_Stock     = true;
            public static bool NavBarGroup_Invoices  = true;
            public static bool NavBarGroup_Finance   = true;
            public static bool NavBarGroup_Cash      = true;
            public static bool NavBarGroup_HR        = true;
            public static bool NavBarGroup_Test      = true;
            public static bool NavBarGroup_Reports   = true;
            public static bool NavBarGroup_Setting   = true;

        #endregion

        #region frm_Customers
        public static bool frm_Customers = true;
        public static bool frm_Customers_Add = true;
        public static bool frm_Customers_Edit = true;
        public static bool frm_Customers_Delete = true;
        public static bool frm_Customers_Print = true;

        #endregion
        // Genreated By ClassMaker
        #region frm_CompanyInfo
        public static bool   frm_CompanyInfo = true;
        public static bool   frm_CompanyInfo_Add = true;
        public static bool   frm_CompanyInfo_Edit = true ;
        public static bool   frm_CompanyInfo_Delete = false ;
        public static bool   frm_CompanyInfo_Print = false ;
        #endregion
        // Genreated By ClassMaker
        #region frm_Item
        public static bool frm_Item = true;
        public static bool frm_Item_Add = true;
        public static bool frm_Item_Edit = true;
        public static bool frm_Item_Delete = true;
        public static bool frm_Item_Print = true;
        #endregion
        // Genreated By ClassMaker
        #region frm_Drawers
        public static bool frm_Drawers = true;
        public static bool frm_Drawers_Add = true;
        public static bool frm_Drawers_Edit = true;
        public static bool frm_Drawers_Delete = true;
        public static bool frm_Drawers_Print = true;
        #endregion
        // Genreated By ClassMaker
        #region frm_Stores
        public static bool frm_Stores = true;
        public static bool frm_Stores_Add = true;
        public static bool frm_Stores_Edit = false ;
        public static bool frm_Stores_Delete = true ;
        public static bool frm_Stores_Print = true ;
        #endregion

        // Genreated By ClassMaker
        #region frm_Users
        public static bool frm_Users = true;
        public static bool frm_Users_Add = true;
        public static bool frm_Users_Edit = true;
        public static bool frm_Users_Delete = true;
        public static bool frm_Users_Print = true;
        #endregion
        // Genreated By ClassMaker
        #region frm_Vendors
        public static bool frm_Vendors = true;
        public static bool frm_Vendors_Add = true;
        public static bool frm_Vendors_Edit = true;
        public static bool frm_Vendors_Delete = true;
        public static bool frm_Vendors_Print = true;
        #endregion
        // Genreated By ClassMaker
        #region frm_SlInvoice
        public static bool frm_SlInvoice = true;
        public static bool frm_SlInvoice_Add = true;
        public static bool frm_SlInvoice_Edit = true;
        public static bool frm_SlInvoice_Delete = true;
        public static bool frm_SlInvoice_Print = true;
        #endregion




        #endregion



        static public bool   FormPrivilege(string Field)
        {
            if (typeof(CurrentUser).GetField(Field) == null) return true;
            else 
            return (bool)  typeof(CurrentUser).GetField(Field).GetValue(new object ());
        }
    }
}
