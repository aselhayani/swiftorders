﻿namespace SwiftOrders.Forms
{
    partial class frm_Shipment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            this.DateEdit_Date = new DevExpress.XtraEditors.DateEdit();
            this.LookUpEdit_Start = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl_Car = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Car = new DevExpress.XtraEditors.LookUpEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl_Measurement_Fuel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_Measurement_ODO = new DevExpress.XtraEditors.LabelControl();
            this.SpinEdit_Measurement_ODO = new DevExpress.XtraEditors.SpinEdit();
            this.SpinEdit_Measurement_Fuel = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Driver = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.label1 = new System.Windows.Forms.Label();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Start.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Car.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_Measurement_ODO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_Measurement_Fuel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Driver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DateEdit_Date
            // 
            this.DateEdit_Date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEdit_Date.EditValue = new System.DateTime(2018, 6, 8, 0, 0, 0, 0);
            this.DateEdit_Date.Location = new System.Drawing.Point(427, 50);
            this.DateEdit_Date.Name = "DateEdit_Date";
            this.DateEdit_Date.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_Date.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_Date.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_Date.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_Date.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_Date.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_Date.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_Date.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_Date.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_Date.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_Date.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_Date.Size = new System.Drawing.Size(230, 20);
            this.DateEdit_Date.TabIndex = 2;
            // 
            // LookUpEdit_Start
            // 
            this.LookUpEdit_Start.Location = new System.Drawing.Point(5, 23);
            this.LookUpEdit_Start.Name = "LookUpEdit_Start";
            this.LookUpEdit_Start.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Start.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Start.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Start.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Start.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Start.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Start.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Start.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Start.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Start.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Start.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Start.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Start.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Start.Properties.NullText = "";
            this.LookUpEdit_Start.Size = new System.Drawing.Size(280, 20);
            this.LookUpEdit_Start.TabIndex = 0;
            // 
            // labelControl_Car
            // 
            this.labelControl_Car.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_Car.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Car.Appearance.Options.UseForeColor = true;
            this.labelControl_Car.Location = new System.Drawing.Point(664, 28);
            this.labelControl_Car.Name = "labelControl_Car";
            this.labelControl_Car.Size = new System.Drawing.Size(34, 13);
            this.labelControl_Car.TabIndex = 1000;
            this.labelControl_Car.Text = "السياره";
            // 
            // LookUpEdit_Car
            // 
            this.LookUpEdit_Car.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_Car.Location = new System.Drawing.Point(427, 25);
            this.LookUpEdit_Car.Name = "LookUpEdit_Car";
            this.LookUpEdit_Car.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Car.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Car.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Car.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Car.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Car.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Car.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Car.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Car.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Car.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Car.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Car.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Car.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Car.Properties.NullText = "";
            this.LookUpEdit_Car.Size = new System.Drawing.Size(230, 20);
            this.LookUpEdit_Car.TabIndex = 0;
            this.LookUpEdit_Car.EditValueChanged += new System.EventHandler(this.LookUpEdit_Car_EditValueChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 20);
            this.gridControl1.LookAndFeel.SkinName = "Metropolis";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(710, 167);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl1_ProcessGridKey);
            // 
            // gridView1
            // 
            this.gridView1.ActiveFilterEnabled = false;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsFind.AllowFindPanel = false;
            this.gridView1.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.CustomRowCellEditForEditing += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEditForEditing);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.DataSourceChanged += new System.EventHandler(this.gridView1_DataSourceChanged);
            // 
            // gridControl3
            // 
            this.gridControl3.Location = new System.Drawing.Point(5, 49);
            this.gridControl3.LookAndFeel.SkinName = "Metropolis";
            this.gridControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(280, 203);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            this.gridControl3.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl3_ProcessGridKey);
            // 
            // gridView3
            // 
            this.gridView3.ActiveFilterEnabled = false;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView3.OptionsCustomization.AllowColumnMoving = false;
            this.gridView3.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView3.OptionsFind.AllowFindPanel = false;
            this.gridView3.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView3.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView3.OptionsView.ShowDetailButtons = false;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.CustomRowCellEditForEditing += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEditForEditing);
            this.gridView3.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView3_CellValueChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 126);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(714, 189);
            this.groupControl1.TabIndex = 1009;
            this.groupControl1.Text = "الطلبيات";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.progressBar1.ForeColor = System.Drawing.Color.Green;
            this.progressBar1.Location = new System.Drawing.Point(2, 205);
            this.progressBar1.MarqueeAnimationSpeed = 500;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(409, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 1007;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.gridControl3);
            this.groupControl2.Controls.Add(this.LookUpEdit_Start);
            this.groupControl2.Location = new System.Drawing.Point(439, 321);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(287, 252);
            this.groupControl2.TabIndex = 1010;
            this.groupControl2.Text = "الواجهه";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton1.Location = new System.Drawing.Point(20, 579);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(83, 32);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "حفظ";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton2.Location = new System.Drawing.Point(109, 579);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(83, 32);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "طباعه";
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.labelControl_Measurement_Fuel);
            this.groupControl3.Controls.Add(this.labelControl3);
            this.groupControl3.Controls.Add(this.labelControl_Measurement_ODO);
            this.groupControl3.Controls.Add(this.SpinEdit_Measurement_ODO);
            this.groupControl3.Controls.Add(this.SpinEdit_Measurement_Fuel);
            this.groupControl3.Controls.Add(this.DateEdit_Date);
            this.groupControl3.Controls.Add(this.labelControl1);
            this.groupControl3.Controls.Add(this.labelControl_Car);
            this.groupControl3.Controls.Add(this.LookUpEdit_Driver);
            this.groupControl3.Controls.Add(this.LookUpEdit_Car);
            this.groupControl3.Location = new System.Drawing.Point(12, 9);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(714, 110);
            this.groupControl3.TabIndex = 1012;
            this.groupControl3.Text = "السياره";
            // 
            // labelControl_Measurement_Fuel
            // 
            this.labelControl_Measurement_Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_Measurement_Fuel.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Measurement_Fuel.Appearance.Options.UseForeColor = true;
            this.labelControl_Measurement_Fuel.Location = new System.Drawing.Point(247, 54);
            this.labelControl_Measurement_Fuel.Name = "labelControl_Measurement_Fuel";
            this.labelControl_Measurement_Fuel.Size = new System.Drawing.Size(106, 13);
            this.labelControl_Measurement_Fuel.TabIndex = 1000;
            this.labelControl_Measurement_Fuel.Text = "استهلاك الوقود السابق\r\n";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(273, 28);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(79, 13);
            this.labelControl3.TabIndex = 1000;
            this.labelControl3.Text = "عداد الكيلو مترات";
            // 
            // labelControl_Measurement_ODO
            // 
            this.labelControl_Measurement_ODO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_Measurement_ODO.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_Measurement_ODO.Appearance.Options.UseForeColor = true;
            this.labelControl_Measurement_ODO.Location = new System.Drawing.Point(670, 52);
            this.labelControl_Measurement_ODO.Name = "labelControl_Measurement_ODO";
            this.labelControl_Measurement_ODO.Size = new System.Drawing.Size(28, 13);
            this.labelControl_Measurement_ODO.TabIndex = 1000;
            this.labelControl_Measurement_ODO.Text = "التاريخ";
            // 
            // SpinEdit_Measurement_ODO
            // 
            this.SpinEdit_Measurement_ODO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SpinEdit_Measurement_ODO.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_Measurement_ODO.Location = new System.Drawing.Point(8, 25);
            this.SpinEdit_Measurement_ODO.Name = "SpinEdit_Measurement_ODO";
            this.SpinEdit_Measurement_ODO.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SpinEdit_Measurement_ODO.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.SpinEdit_Measurement_ODO.Properties.Appearance.Options.UseBackColor = true;
            this.SpinEdit_Measurement_ODO.Properties.Appearance.Options.UseBorderColor = true;
            this.SpinEdit_Measurement_ODO.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.SpinEdit_Measurement_ODO.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.SpinEdit_Measurement_ODO.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.SpinEdit_Measurement_ODO.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.SpinEdit_Measurement_ODO.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.SpinEdit_Measurement_ODO.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpinEdit_Measurement_ODO.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.SpinEdit_Measurement_ODO.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SpinEdit_Measurement_ODO.Size = new System.Drawing.Size(230, 20);
            this.SpinEdit_Measurement_ODO.TabIndex = 1;
            // 
            // SpinEdit_Measurement_Fuel
            // 
            this.SpinEdit_Measurement_Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SpinEdit_Measurement_Fuel.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_Measurement_Fuel.Location = new System.Drawing.Point(8, 51);
            this.SpinEdit_Measurement_Fuel.Name = "SpinEdit_Measurement_Fuel";
            this.SpinEdit_Measurement_Fuel.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SpinEdit_Measurement_Fuel.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.SpinEdit_Measurement_Fuel.Properties.Appearance.Options.UseBackColor = true;
            this.SpinEdit_Measurement_Fuel.Properties.Appearance.Options.UseBorderColor = true;
            this.SpinEdit_Measurement_Fuel.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.SpinEdit_Measurement_Fuel.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.SpinEdit_Measurement_Fuel.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.SpinEdit_Measurement_Fuel.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.SpinEdit_Measurement_Fuel.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.SpinEdit_Measurement_Fuel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpinEdit_Measurement_Fuel.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.SpinEdit_Measurement_Fuel.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SpinEdit_Measurement_Fuel.Size = new System.Drawing.Size(230, 20);
            this.SpinEdit_Measurement_Fuel.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(663, 78);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(34, 13);
            this.labelControl1.TabIndex = 1000;
            this.labelControl1.Text = "السائق";
            // 
            // LookUpEdit_Driver
            // 
            this.LookUpEdit_Driver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_Driver.Location = new System.Drawing.Point(427, 74);
            this.LookUpEdit_Driver.Name = "LookUpEdit_Driver";
            this.LookUpEdit_Driver.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Driver.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Driver.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Driver.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Driver.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Driver.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Driver.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Driver.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Driver.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Driver.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Driver.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Driver.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Driver.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Driver.Properties.NullText = "";
            this.LookUpEdit_Driver.Size = new System.Drawing.Size(231, 20);
            this.LookUpEdit_Driver.TabIndex = 0;
            this.LookUpEdit_Driver.EditValueChanged += new System.EventHandler(this.LookUpEdit_Car_EditValueChanged);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton3.Location = new System.Drawing.Point(198, 579);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(112, 32);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "طباعه اذن التحميل";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.label1);
            this.groupControl4.Controls.Add(this.gridControl4);
            this.groupControl4.Controls.Add(this.progressBar1);
            this.groupControl4.Location = new System.Drawing.Point(12, 319);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(413, 254);
            this.groupControl4.TabIndex = 1010;
            this.groupControl4.Text = "اجمالي الحموله";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(2, 228);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(409, 21);
            this.label1.TabIndex = 1008;
            this.label1.Text = "         ";
            // 
            // gridControl4
            // 
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridControl4.Location = new System.Drawing.Point(2, 20);
            this.gridControl4.LookAndFeel.SkinName = "Metropolis";
            this.gridControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(409, 184);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            this.gridControl4.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl4_ProcessGridKey);
            // 
            // gridView4
            // 
            this.gridView4.ActiveFilterEnabled = false;
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView4.OptionsCustomization.AllowColumnMoving = false;
            this.gridView4.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView4.OptionsFind.AllowFindPanel = false;
            this.gridView4.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView4.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView4.OptionsView.ShowDetailButtons = false;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowFooter = true;
            this.gridView4.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.CustomRowCellEditForEditing += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView4_CustomRowCellEditForEditing);
            this.gridView4.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView4_InitNewRow);
            this.gridView4.ColumnChanged += new System.EventHandler(this.gridView4_ColumnChanged);
            this.gridView4.FocusedRowObjectChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowObjectChangedEventHandler(this.gridView4_FocusedRowObjectChanged);
            this.gridView4.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView4_CellValueChanged);
            this.gridView4.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView4_ValidateRow);
            this.gridView4.Click += new System.EventHandler(this.gridView4_Click);
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit1.Location = new System.Drawing.Point(316, 585);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AutoWidth = true;
            this.checkEdit1.Properties.Caption = "انشاء فاتوره مبيعات للطلبيات";
            this.checkEdit1.Size = new System.Drawing.Size(152, 19);
            this.checkEdit1.TabIndex = 1013;
            // 
            // frm_Shipment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(740, 615);
            this.Controls.Add(this.checkEdit1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Name = "frm_Shipment";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "مأموريه جديده";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_Shipment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Start.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Car.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_Measurement_ODO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_Measurement_Fuel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Driver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.DateEdit DateEdit_Date;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Start;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Car;
        DevExpress.XtraEditors.LabelControl labelControl_Car;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl1;

        private DevExpress.XtraEditors.SpinEdit SpinEdit_Measurement_ODO;
        DevExpress.XtraEditors.LabelControl labelControl_Measurement_ODO;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_Measurement_Fuel;
        DevExpress.XtraEditors.LabelControl labelControl_Measurement_Fuel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Driver;
    }
}