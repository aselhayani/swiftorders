﻿namespace SwiftOrders.Forms
{
    partial class frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.الطلبياتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.طلبيهجديدهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.عرضToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الكلToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.بانتظارالمراجعهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.بانتظرتبليغالعميلToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.بانتظارالتسليمToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تمتسليمهاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.السياراتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اضافهتعديلToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.السائقونToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تسجيلقراءهعدادToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الواجهاتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.التقاريرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مسحوباتالعملاءToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.التشغيلToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.طباعهامرتشغيلToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.المأمورياتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.جديدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.عرضToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.اعداداتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اعادهتصميمالتقاريرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.تحديثToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TextEdit_Ton = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_Today = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_1 = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_0 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.alertControl1 = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Ton.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Today.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_0.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.الطلبياتToolStripMenuItem,
            this.السياراتToolStripMenuItem,
            this.الواجهاتToolStripMenuItem,
            this.التقاريرToolStripMenuItem,
            this.التشغيلToolStripMenuItem,
            this.المأمورياتToolStripMenuItem,
            this.اعداداتToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(900, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // الطلبياتToolStripMenuItem
            // 
            this.الطلبياتToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.طلبيهجديدهToolStripMenuItem,
            this.عرضToolStripMenuItem});
            this.الطلبياتToolStripMenuItem.Name = "الطلبياتToolStripMenuItem";
            this.الطلبياتToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.الطلبياتToolStripMenuItem.Text = "الطلبيات";
            // 
            // طلبيهجديدهToolStripMenuItem
            // 
            this.طلبيهجديدهToolStripMenuItem.Name = "طلبيهجديدهToolStripMenuItem";
            this.طلبيهجديدهToolStripMenuItem.ShortcutKeyDisplayString = "Alt+N";
            this.طلبيهجديدهToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.طلبيهجديدهToolStripMenuItem.Text = "طلبيه جديده ";
            this.طلبيهجديدهToolStripMenuItem.Click += new System.EventHandler(this.طلبيهجديدهToolStripMenuItem_Click);
            // 
            // عرضToolStripMenuItem
            // 
            this.عرضToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.الكلToolStripMenuItem,
            this.بانتظارالمراجعهToolStripMenuItem,
            this.بانتظرتبليغالعميلToolStripMenuItem,
            this.بانتظارالتسليمToolStripMenuItem,
            this.تمتسليمهاToolStripMenuItem});
            this.عرضToolStripMenuItem.Name = "عرضToolStripMenuItem";
            this.عرضToolStripMenuItem.ShortcutKeyDisplayString = "A-R-S-D-F";
            this.عرضToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.عرضToolStripMenuItem.Text = "عرض";
            // 
            // الكلToolStripMenuItem
            // 
            this.الكلToolStripMenuItem.Name = "الكلToolStripMenuItem";
            this.الكلToolStripMenuItem.ShortcutKeyDisplayString = "Alt+A";
            this.الكلToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.الكلToolStripMenuItem.Text = "الكل ";
            this.الكلToolStripMenuItem.Click += new System.EventHandler(this.الكلToolStripMenuItem_Click);
            // 
            // بانتظارالمراجعهToolStripMenuItem
            // 
            this.بانتظارالمراجعهToolStripMenuItem.Name = "بانتظارالمراجعهToolStripMenuItem";
            this.بانتظارالمراجعهToolStripMenuItem.ShortcutKeyDisplayString = "Alt+R";
            this.بانتظارالمراجعهToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.بانتظارالمراجعهToolStripMenuItem.Text = "بانتظار المراجعه ";
            this.بانتظارالمراجعهToolStripMenuItem.Click += new System.EventHandler(this.بانتظارالمراجعهToolStripMenuItem_Click);
            // 
            // بانتظرتبليغالعميلToolStripMenuItem
            // 
            this.بانتظرتبليغالعميلToolStripMenuItem.Name = "بانتظرتبليغالعميلToolStripMenuItem";
            this.بانتظرتبليغالعميلToolStripMenuItem.ShortcutKeyDisplayString = "Alt+S";
            this.بانتظرتبليغالعميلToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.بانتظرتبليغالعميلToolStripMenuItem.Text = "بانتظر تبليغ العميل ";
            this.بانتظرتبليغالعميلToolStripMenuItem.Click += new System.EventHandler(this.بانتظرتبليغالعميلToolStripMenuItem_Click);
            // 
            // بانتظارالتسليمToolStripMenuItem
            // 
            this.بانتظارالتسليمToolStripMenuItem.Name = "بانتظارالتسليمToolStripMenuItem";
            this.بانتظارالتسليمToolStripMenuItem.ShortcutKeyDisplayString = "Alt+D";
            this.بانتظارالتسليمToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.بانتظارالتسليمToolStripMenuItem.Text = "بانتظار التسليم ";
            this.بانتظارالتسليمToolStripMenuItem.Click += new System.EventHandler(this.بانتظارالتسليمToolStripMenuItem_Click);
            // 
            // تمتسليمهاToolStripMenuItem
            // 
            this.تمتسليمهاToolStripMenuItem.Name = "تمتسليمهاToolStripMenuItem";
            this.تمتسليمهاToolStripMenuItem.ShortcutKeyDisplayString = "Alt+F";
            this.تمتسليمهاToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.تمتسليمهاToolStripMenuItem.Text = "تم تسليمها ";
            this.تمتسليمهاToolStripMenuItem.Click += new System.EventHandler(this.تمتسليمهاToolStripMenuItem_Click);
            // 
            // السياراتToolStripMenuItem
            // 
            this.السياراتToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.اضافهتعديلToolStripMenuItem,
            this.السائقونToolStripMenuItem,
            this.تسجيلقراءهعدادToolStripMenuItem});
            this.السياراتToolStripMenuItem.Name = "السياراتToolStripMenuItem";
            this.السياراتToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.السياراتToolStripMenuItem.Text = "السيارات";
            // 
            // اضافهتعديلToolStripMenuItem
            // 
            this.اضافهتعديلToolStripMenuItem.Name = "اضافهتعديلToolStripMenuItem";
            this.اضافهتعديلToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.اضافهتعديلToolStripMenuItem.Text = "اضافه / تعديل";
            this.اضافهتعديلToolStripMenuItem.Click += new System.EventHandler(this.اضافهتعديلToolStripMenuItem_Click);
            // 
            // السائقونToolStripMenuItem
            // 
            this.السائقونToolStripMenuItem.Name = "السائقونToolStripMenuItem";
            this.السائقونToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.السائقونToolStripMenuItem.Text = "السائقون";
            this.السائقونToolStripMenuItem.Click += new System.EventHandler(this.السائقونToolStripMenuItem_Click);
            // 
            // تسجيلقراءهعدادToolStripMenuItem
            // 
            this.تسجيلقراءهعدادToolStripMenuItem.Name = "تسجيلقراءهعدادToolStripMenuItem";
            this.تسجيلقراءهعدادToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.تسجيلقراءهعدادToolStripMenuItem.Text = "تسجيل قراءه عداد";
            this.تسجيلقراءهعدادToolStripMenuItem.Click += new System.EventHandler(this.تسجيلقراءهعدادToolStripMenuItem_Click);
            // 
            // الواجهاتToolStripMenuItem
            // 
            this.الواجهاتToolStripMenuItem.Name = "الواجهاتToolStripMenuItem";
            this.الواجهاتToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.الواجهاتToolStripMenuItem.Text = "الواجهات";
            this.الواجهاتToolStripMenuItem.Click += new System.EventHandler(this.الواجهاتToolStripMenuItem_Click);
            // 
            // التقاريرToolStripMenuItem
            // 
            this.التقاريرToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.مسحوباتالعملاءToolStripMenuItem});
            this.التقاريرToolStripMenuItem.Name = "التقاريرToolStripMenuItem";
            this.التقاريرToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.التقاريرToolStripMenuItem.Text = "التقارير";
            // 
            // مسحوباتالعملاءToolStripMenuItem
            // 
            this.مسحوباتالعملاءToolStripMenuItem.Name = "مسحوباتالعملاءToolStripMenuItem";
            this.مسحوباتالعملاءToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.مسحوباتالعملاءToolStripMenuItem.Text = "مسحوبات العملاء";
            this.مسحوباتالعملاءToolStripMenuItem.Click += new System.EventHandler(this.مسحوباتالعملاءToolStripMenuItem_Click);
            // 
            // التشغيلToolStripMenuItem
            // 
            this.التشغيلToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.طباعهامرتشغيلToolStripMenuItem});
            this.التشغيلToolStripMenuItem.Name = "التشغيلToolStripMenuItem";
            this.التشغيلToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.التشغيلToolStripMenuItem.Text = "التشغيل";
            // 
            // طباعهامرتشغيلToolStripMenuItem
            // 
            this.طباعهامرتشغيلToolStripMenuItem.Name = "طباعهامرتشغيلToolStripMenuItem";
            this.طباعهامرتشغيلToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.طباعهامرتشغيلToolStripMenuItem.Text = "طباعه امر تشغيل";
            this.طباعهامرتشغيلToolStripMenuItem.Click += new System.EventHandler(this.طباعهامرتشغيلToolStripMenuItem_Click);
            // 
            // المأمورياتToolStripMenuItem
            // 
            this.المأمورياتToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.جديدToolStripMenuItem,
            this.عرضToolStripMenuItem1});
            this.المأمورياتToolStripMenuItem.Name = "المأمورياتToolStripMenuItem";
            this.المأمورياتToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.المأمورياتToolStripMenuItem.Text = "المأموريات";
            // 
            // جديدToolStripMenuItem
            // 
            this.جديدToolStripMenuItem.Name = "جديدToolStripMenuItem";
            this.جديدToolStripMenuItem.Size = new System.Drawing.Size(101, 22);
            this.جديدToolStripMenuItem.Text = "جديد";
            this.جديدToolStripMenuItem.Click += new System.EventHandler(this.جديدToolStripMenuItem_Click);
            // 
            // عرضToolStripMenuItem1
            // 
            this.عرضToolStripMenuItem1.Name = "عرضToolStripMenuItem1";
            this.عرضToolStripMenuItem1.Size = new System.Drawing.Size(101, 22);
            this.عرضToolStripMenuItem1.Text = "عرض";
            this.عرضToolStripMenuItem1.Click += new System.EventHandler(this.عرضToolStripMenuItem1_Click);
            // 
            // اعداداتToolStripMenuItem
            // 
            this.اعداداتToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.اعادهتصميمالتقاريرToolStripMenuItem});
            this.اعداداتToolStripMenuItem.Name = "اعداداتToolStripMenuItem";
            this.اعداداتToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.اعداداتToolStripMenuItem.Text = "اعدادات";
            // 
            // اعادهتصميمالتقاريرToolStripMenuItem
            // 
            this.اعادهتصميمالتقاريرToolStripMenuItem.Name = "اعادهتصميمالتقاريرToolStripMenuItem";
            this.اعادهتصميمالتقاريرToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.اعادهتصميمالتقاريرToolStripMenuItem.Text = "اعاده تصميم التقارير";
            this.اعادهتصميمالتقاريرToolStripMenuItem.Click += new System.EventHandler(this.اعادهتصميمالتقاريرToolStripMenuItem_Click);
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.ContextMenuStrip = this.contextMenuStrip1;
            this.xtraScrollableControl1.Controls.Add(this.groupControl1);
            this.xtraScrollableControl1.Controls.Add(this.gridControl1);
            this.xtraScrollableControl1.Controls.Add(this.TextEdit_Ton);
            this.xtraScrollableControl1.Controls.Add(this.textEdit_Today);
            this.xtraScrollableControl1.Controls.Add(this.TextEdit_1);
            this.xtraScrollableControl1.Controls.Add(this.TextEdit_0);
            this.xtraScrollableControl1.Controls.Add(this.labelControl3);
            this.xtraScrollableControl1.Controls.Add(this.labelControl2);
            this.xtraScrollableControl1.Controls.Add(this.labelControl1);
            this.xtraScrollableControl1.Location = new System.Drawing.Point(10, 46);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(244, 526);
            this.xtraScrollableControl1.TabIndex = 0;
            this.xtraScrollableControl1.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.تحديثToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.contextMenuStrip1.Size = new System.Drawing.Size(107, 26);
            // 
            // تحديثToolStripMenuItem
            // 
            this.تحديثToolStripMenuItem.Name = "تحديثToolStripMenuItem";
            this.تحديثToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.تحديثToolStripMenuItem.Text = "تحديث";
            this.تحديثToolStripMenuItem.Click += new System.EventHandler(this.تحديثToolStripMenuItem_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl2);
            this.groupControl1.Location = new System.Drawing.Point(5, 354);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(235, 172);
            this.groupControl1.TabIndex = 1004;
            this.groupControl1.Text = "حموله السيارت اليوم ";
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(2, 20);
            this.gridControl2.LookAndFeel.SkinName = "Metropolis";
            this.gridControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(231, 150);
            this.gridControl2.TabIndex = 1004;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.ActiveFilterEnabled = false;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsCustomization.AllowColumnMoving = false;
            this.gridView2.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView2.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView2.OptionsView.ShowColumnHeaders = false;
            this.gridView2.OptionsView.ShowDetailButtons = false;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(5, 189);
            this.gridControl1.LookAndFeel.SkinName = "Metropolis";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(235, 159);
            this.gridControl1.TabIndex = 1003;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ActiveFilterEnabled = false;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView1.OptionsView.ShowColumnHeaders = false;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // TextEdit_Ton
            // 
            this.TextEdit_Ton.Location = new System.Drawing.Point(5, 161);
            this.TextEdit_Ton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextEdit_Ton.Name = "TextEdit_Ton";
            this.TextEdit_Ton.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_Ton.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_Ton.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_Ton.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_Ton.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_Ton.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TextEdit_Ton.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TextEdit_Ton.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_Ton.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_Ton.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_Ton.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_Ton.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_Ton.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_Ton.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_Ton.Size = new System.Drawing.Size(131, 20);
            this.TextEdit_Ton.TabIndex = 1001;
            // 
            // textEdit_Today
            // 
            this.textEdit_Today.Location = new System.Drawing.Point(106, 161);
            this.textEdit_Today.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textEdit_Today.Name = "textEdit_Today";
            this.textEdit_Today.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textEdit_Today.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.textEdit_Today.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit_Today.Properties.Appearance.Options.UseBorderColor = true;
            this.textEdit_Today.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit_Today.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit_Today.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit_Today.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.textEdit_Today.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.textEdit_Today.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.textEdit_Today.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.textEdit_Today.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit_Today.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.textEdit_Today.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.textEdit_Today.Size = new System.Drawing.Size(135, 20);
            this.textEdit_Today.TabIndex = 1001;
            // 
            // TextEdit_1
            // 
            this.TextEdit_1.Location = new System.Drawing.Point(4, 99);
            this.TextEdit_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextEdit_1.Name = "TextEdit_1";
            this.TextEdit_1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_1.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_1.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_1.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_1.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TextEdit_1.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TextEdit_1.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_1.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_1.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_1.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_1.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_1.Size = new System.Drawing.Size(236, 20);
            this.TextEdit_1.TabIndex = 1001;
            // 
            // TextEdit_0
            // 
            this.TextEdit_0.Location = new System.Drawing.Point(4, 37);
            this.TextEdit_0.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextEdit_0.Name = "TextEdit_0";
            this.TextEdit_0.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_0.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_0.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_0.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_0.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_0.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TextEdit_0.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TextEdit_0.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_0.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_0.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_0.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_0.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_0.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_0.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_0.Size = new System.Drawing.Size(236, 20);
            this.TextEdit_0.TabIndex = 1001;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelControl3.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl3.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseBorderColor = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl3.Location = new System.Drawing.Point(5, 129);
            this.labelControl3.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(236, 31);
            this.labelControl3.TabIndex = 1002;
            this.labelControl3.Text = "سوف تسلم اليوم";
            this.labelControl3.Click += new System.EventHandler(this.labelControl3_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl2.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labelControl2.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseBorderColor = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl2.Location = new System.Drawing.Point(4, 67);
            this.labelControl2.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(236, 31);
            this.labelControl2.TabIndex = 1002;
            this.labelControl2.Text = "تحتاج الي تبليغ العميل ";
            this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelControl1.Appearance.BorderColor = System.Drawing.Color.Gray;
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseBorderColor = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl1.Location = new System.Drawing.Point(4, 5);
            this.labelControl1.LookAndFeel.SkinName = "Seven Classic";
            this.labelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(236, 31);
            this.labelControl1.TabIndex = 1002;
            this.labelControl1.Text = "تحتاج الي مراجعه ";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // alertControl1
            // 
            this.alertControl1.FormMaxCount = 10;
            this.alertControl1.AlertClick += new DevExpress.XtraBars.Alerter.AlertClickEventHandler(this.alertControl1_AlertClick);
            // 
            // frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 487);
            this.Controls.Add(this.xtraScrollableControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frm_Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "الشاشه الرئيسيه";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Main_FormClosing);
            this.Load += new System.EventHandler(this.frm_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_Main_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_Main_KeyPress);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Ton.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Today.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_0.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem الطلبياتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem طلبيهجديدهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem السياراتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اضافهتعديلToolStripMenuItem;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private System.Windows.Forms.ToolStripMenuItem عرضToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الكلToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem بانتظارالمراجعهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem بانتظرتبليغالعميلToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem بانتظارالتسليمToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تمتسليمهاToolStripMenuItem;
        //private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.TextEdit TextEdit_0;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.TextEdit TextEdit_Ton;
        private DevExpress.XtraEditors.TextEdit textEdit_Today;
        private DevExpress.XtraEditors.TextEdit TextEdit_1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem تحديثToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الواجهاتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem التقاريرToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem مسحوباتالعملاءToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تسجيلقراءهعدادToolStripMenuItem;
        private DevExpress.XtraBars.Alerter.AlertControl alertControl1;
        private System.Windows.Forms.ToolStripMenuItem التشغيلToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem طباعهامرتشغيلToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem السائقونToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem المأمورياتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem جديدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem عرضToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem اعداداتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اعادهتصميمالتقاريرToolStripMenuItem;
    }
}