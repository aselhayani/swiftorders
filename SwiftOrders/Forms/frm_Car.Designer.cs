﻿namespace SwiftOrders.Forms
{
    partial class frm_Car
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl_PlateNB = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_PlateNB = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_CargoCap = new DevExpress.XtraEditors.LabelControl();
            this.SpinEdit_CargoCap = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl_TankCap = new DevExpress.XtraEditors.LabelControl();
            this.SpinEdit_TankCap = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl_CarColor = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_CarColor = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_EngSerial = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_EngSerial = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_ChassisNumber = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_ChassisNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_ModelYear = new DevExpress.XtraEditors.LabelControl();
            this.DateEdit_ModelYear = new DevExpress.XtraEditors.DateEdit();
            this.labelControl_ModelNumber = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_ModelNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_CurrentRegstrationStartDate = new DevExpress.XtraEditors.LabelControl();
            this.DateEdit_CurrentRegstrationStartDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl_CurrentRegstrationEndDate = new DevExpress.XtraEditors.LabelControl();
            this.DateEdit_CurrentRegstrationEndDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl_TireSize = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_TireSize = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Save = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Driver = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_PlateNB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_CargoCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_TankCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_CarColor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_EngSerial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ChassisNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_ModelYear.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_ModelYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ModelNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_CurrentRegstrationStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_CurrentRegstrationStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_CurrentRegstrationEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_CurrentRegstrationEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_TireSize.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Driver.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl_PlateNB
            // 
            this.labelControl_PlateNB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_PlateNB.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_PlateNB.Appearance.Options.UseForeColor = true;
            this.labelControl_PlateNB.Location = new System.Drawing.Point(246, 23);
            this.labelControl_PlateNB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_PlateNB.Name = "labelControl_PlateNB";
            this.labelControl_PlateNB.Size = new System.Drawing.Size(60, 17);
            this.labelControl_PlateNB.TabIndex = 1000;
            this.labelControl_PlateNB.Text = "رقم اللوحه";
            // 
            // TextEdit_PlateNB
            // 
            this.TextEdit_PlateNB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_PlateNB.Location = new System.Drawing.Point(13, 20);
            this.TextEdit_PlateNB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TextEdit_PlateNB.Name = "TextEdit_PlateNB";
            this.TextEdit_PlateNB.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_PlateNB.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_PlateNB.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_PlateNB.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_PlateNB.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_PlateNB.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_PlateNB.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_PlateNB.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_PlateNB.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_PlateNB.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_PlateNB.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_PlateNB.Size = new System.Drawing.Size(173, 22);
            this.TextEdit_PlateNB.TabIndex = 7;
            // 
            // labelControl_CargoCap
            // 
            this.labelControl_CargoCap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_CargoCap.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_CargoCap.Appearance.Options.UseForeColor = true;
            this.labelControl_CargoCap.Location = new System.Drawing.Point(199, 217);
            this.labelControl_CargoCap.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_CargoCap.Name = "labelControl_CargoCap";
            this.labelControl_CargoCap.Size = new System.Drawing.Size(116, 17);
            this.labelControl_CargoCap.TabIndex = 1000;
            this.labelControl_CargoCap.Text = "سعه الحموله بالكيلو";
            // 
            // SpinEdit_CargoCap
            // 
            this.SpinEdit_CargoCap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SpinEdit_CargoCap.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_CargoCap.Location = new System.Drawing.Point(13, 213);
            this.SpinEdit_CargoCap.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SpinEdit_CargoCap.Name = "SpinEdit_CargoCap";
            this.SpinEdit_CargoCap.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SpinEdit_CargoCap.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.SpinEdit_CargoCap.Properties.Appearance.Options.UseBackColor = true;
            this.SpinEdit_CargoCap.Properties.Appearance.Options.UseBorderColor = true;
            this.SpinEdit_CargoCap.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.SpinEdit_CargoCap.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.SpinEdit_CargoCap.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.SpinEdit_CargoCap.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.SpinEdit_CargoCap.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.SpinEdit_CargoCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpinEdit_CargoCap.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.SpinEdit_CargoCap.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SpinEdit_CargoCap.Size = new System.Drawing.Size(173, 22);
            this.SpinEdit_CargoCap.TabIndex = 7;
            // 
            // labelControl_TankCap
            // 
            this.labelControl_TankCap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_TankCap.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_TankCap.Appearance.Options.UseForeColor = true;
            this.labelControl_TankCap.Location = new System.Drawing.Point(212, 253);
            this.labelControl_TankCap.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_TankCap.Name = "labelControl_TankCap";
            this.labelControl_TankCap.Size = new System.Drawing.Size(96, 17);
            this.labelControl_TankCap.TabIndex = 1000;
            this.labelControl_TankCap.Text = "سعه خزان الوقود";
            // 
            // SpinEdit_TankCap
            // 
            this.SpinEdit_TankCap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SpinEdit_TankCap.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_TankCap.Location = new System.Drawing.Point(13, 250);
            this.SpinEdit_TankCap.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SpinEdit_TankCap.Name = "SpinEdit_TankCap";
            this.SpinEdit_TankCap.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SpinEdit_TankCap.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.SpinEdit_TankCap.Properties.Appearance.Options.UseBackColor = true;
            this.SpinEdit_TankCap.Properties.Appearance.Options.UseBorderColor = true;
            this.SpinEdit_TankCap.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.SpinEdit_TankCap.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.SpinEdit_TankCap.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.SpinEdit_TankCap.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.SpinEdit_TankCap.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.SpinEdit_TankCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpinEdit_TankCap.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.SpinEdit_TankCap.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SpinEdit_TankCap.Size = new System.Drawing.Size(173, 22);
            this.SpinEdit_TankCap.TabIndex = 7;
            // 
            // labelControl_CarColor
            // 
            this.labelControl_CarColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_CarColor.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_CarColor.Appearance.Options.UseForeColor = true;
            this.labelControl_CarColor.Location = new System.Drawing.Point(276, 55);
            this.labelControl_CarColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_CarColor.Name = "labelControl_CarColor";
            this.labelControl_CarColor.Size = new System.Drawing.Size(27, 17);
            this.labelControl_CarColor.TabIndex = 1000;
            this.labelControl_CarColor.Text = "اللون";
            // 
            // TextEdit_CarColor
            // 
            this.TextEdit_CarColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_CarColor.Location = new System.Drawing.Point(13, 52);
            this.TextEdit_CarColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TextEdit_CarColor.Name = "TextEdit_CarColor";
            this.TextEdit_CarColor.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_CarColor.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_CarColor.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_CarColor.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_CarColor.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_CarColor.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_CarColor.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_CarColor.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_CarColor.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_CarColor.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_CarColor.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_CarColor.Size = new System.Drawing.Size(173, 22);
            this.TextEdit_CarColor.TabIndex = 7;
            // 
            // labelControl_EngSerial
            // 
            this.labelControl_EngSerial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_EngSerial.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_EngSerial.Appearance.Options.UseForeColor = true;
            this.labelControl_EngSerial.Location = new System.Drawing.Point(243, 297);
            this.labelControl_EngSerial.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_EngSerial.Name = "labelControl_EngSerial";
            this.labelControl_EngSerial.Size = new System.Drawing.Size(64, 17);
            this.labelControl_EngSerial.TabIndex = 1000;
            this.labelControl_EngSerial.Text = "رقم الماتور ";
            // 
            // TextEdit_EngSerial
            // 
            this.TextEdit_EngSerial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_EngSerial.Location = new System.Drawing.Point(13, 293);
            this.TextEdit_EngSerial.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TextEdit_EngSerial.Name = "TextEdit_EngSerial";
            this.TextEdit_EngSerial.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_EngSerial.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_EngSerial.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_EngSerial.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_EngSerial.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_EngSerial.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_EngSerial.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_EngSerial.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_EngSerial.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_EngSerial.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_EngSerial.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_EngSerial.Size = new System.Drawing.Size(173, 22);
            this.TextEdit_EngSerial.TabIndex = 7;
            // 
            // labelControl_ChassisNumber
            // 
            this.labelControl_ChassisNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_ChassisNumber.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_ChassisNumber.Appearance.Options.UseForeColor = true;
            this.labelControl_ChassisNumber.Location = new System.Drawing.Point(226, 333);
            this.labelControl_ChassisNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_ChassisNumber.Name = "labelControl_ChassisNumber";
            this.labelControl_ChassisNumber.Size = new System.Drawing.Size(82, 17);
            this.labelControl_ChassisNumber.TabIndex = 1000;
            this.labelControl_ChassisNumber.Text = "رقم الشاسيه ";
            // 
            // TextEdit_ChassisNumber
            // 
            this.TextEdit_ChassisNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_ChassisNumber.Location = new System.Drawing.Point(13, 330);
            this.TextEdit_ChassisNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TextEdit_ChassisNumber.Name = "TextEdit_ChassisNumber";
            this.TextEdit_ChassisNumber.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_ChassisNumber.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_ChassisNumber.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_ChassisNumber.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_ChassisNumber.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_ChassisNumber.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_ChassisNumber.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_ChassisNumber.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_ChassisNumber.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_ChassisNumber.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_ChassisNumber.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_ChassisNumber.Size = new System.Drawing.Size(173, 22);
            this.TextEdit_ChassisNumber.TabIndex = 7;
            // 
            // labelControl_ModelYear
            // 
            this.labelControl_ModelYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_ModelYear.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_ModelYear.Appearance.Options.UseForeColor = true;
            this.labelControl_ModelYear.Location = new System.Drawing.Point(234, 116);
            this.labelControl_ModelYear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_ModelYear.Name = "labelControl_ModelYear";
            this.labelControl_ModelYear.Size = new System.Drawing.Size(74, 17);
            this.labelControl_ModelYear.TabIndex = 1000;
            this.labelControl_ModelYear.Text = "سنه الموديل";
            // 
            // DateEdit_ModelYear
            // 
            this.DateEdit_ModelYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEdit_ModelYear.EditValue = new System.DateTime(2018, 5, 18, 0, 0, 0, 0);
            this.DateEdit_ModelYear.Location = new System.Drawing.Point(13, 112);
            this.DateEdit_ModelYear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DateEdit_ModelYear.Name = "DateEdit_ModelYear";
            this.DateEdit_ModelYear.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_ModelYear.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_ModelYear.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_ModelYear.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_ModelYear.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_ModelYear.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_ModelYear.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_ModelYear.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_ModelYear.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_ModelYear.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_ModelYear.Properties.CalendarTimeProperties.Mask.EditMask = "yyyy";
            this.DateEdit_ModelYear.Properties.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat = true;
            this.DateEdit_ModelYear.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_ModelYear.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_ModelYear.Properties.Mask.EditMask = "yyyy";
            this.DateEdit_ModelYear.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateEdit_ModelYear.Size = new System.Drawing.Size(173, 22);
            this.DateEdit_ModelYear.TabIndex = 7;
            // 
            // labelControl_ModelNumber
            // 
            this.labelControl_ModelNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_ModelNumber.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_ModelNumber.Appearance.Options.UseForeColor = true;
            this.labelControl_ModelNumber.Location = new System.Drawing.Point(239, 153);
            this.labelControl_ModelNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_ModelNumber.Name = "labelControl_ModelNumber";
            this.labelControl_ModelNumber.Size = new System.Drawing.Size(68, 17);
            this.labelControl_ModelNumber.TabIndex = 1000;
            this.labelControl_ModelNumber.Text = "رقم الموديل";
            // 
            // TextEdit_ModelNumber
            // 
            this.TextEdit_ModelNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_ModelNumber.Location = new System.Drawing.Point(13, 149);
            this.TextEdit_ModelNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TextEdit_ModelNumber.Name = "TextEdit_ModelNumber";
            this.TextEdit_ModelNumber.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_ModelNumber.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_ModelNumber.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_ModelNumber.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_ModelNumber.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_ModelNumber.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_ModelNumber.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_ModelNumber.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_ModelNumber.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_ModelNumber.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_ModelNumber.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_ModelNumber.Size = new System.Drawing.Size(173, 22);
            this.TextEdit_ModelNumber.TabIndex = 7;
            // 
            // labelControl_CurrentRegstrationStartDate
            // 
            this.labelControl_CurrentRegstrationStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_CurrentRegstrationStartDate.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_CurrentRegstrationStartDate.Appearance.Options.UseForeColor = true;
            this.labelControl_CurrentRegstrationStartDate.Location = new System.Drawing.Point(229, 365);
            this.labelControl_CurrentRegstrationStartDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_CurrentRegstrationStartDate.Name = "labelControl_CurrentRegstrationStartDate";
            this.labelControl_CurrentRegstrationStartDate.Size = new System.Drawing.Size(79, 17);
            this.labelControl_CurrentRegstrationStartDate.TabIndex = 1000;
            this.labelControl_CurrentRegstrationStartDate.Text = "تاريخ الترخيص";
            // 
            // DateEdit_CurrentRegstrationStartDate
            // 
            this.DateEdit_CurrentRegstrationStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEdit_CurrentRegstrationStartDate.EditValue = new System.DateTime(2018, 5, 18, 0, 0, 0, 0);
            this.DateEdit_CurrentRegstrationStartDate.Location = new System.Drawing.Point(13, 362);
            this.DateEdit_CurrentRegstrationStartDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DateEdit_CurrentRegstrationStartDate.Name = "DateEdit_CurrentRegstrationStartDate";
            this.DateEdit_CurrentRegstrationStartDate.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_CurrentRegstrationStartDate.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_CurrentRegstrationStartDate.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_CurrentRegstrationStartDate.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_CurrentRegstrationStartDate.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_CurrentRegstrationStartDate.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_CurrentRegstrationStartDate.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_CurrentRegstrationStartDate.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_CurrentRegstrationStartDate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_CurrentRegstrationStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_CurrentRegstrationStartDate.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_CurrentRegstrationStartDate.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_CurrentRegstrationStartDate.Size = new System.Drawing.Size(173, 22);
            this.DateEdit_CurrentRegstrationStartDate.TabIndex = 7;
            // 
            // labelControl_CurrentRegstrationEndDate
            // 
            this.labelControl_CurrentRegstrationEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_CurrentRegstrationEndDate.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_CurrentRegstrationEndDate.Appearance.Options.UseForeColor = true;
            this.labelControl_CurrentRegstrationEndDate.Location = new System.Drawing.Point(198, 402);
            this.labelControl_CurrentRegstrationEndDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_CurrentRegstrationEndDate.Name = "labelControl_CurrentRegstrationEndDate";
            this.labelControl_CurrentRegstrationEndDate.Size = new System.Drawing.Size(112, 17);
            this.labelControl_CurrentRegstrationEndDate.TabIndex = 1000;
            this.labelControl_CurrentRegstrationEndDate.Text = "تاريخ انتهاء الترخيص";
            // 
            // DateEdit_CurrentRegstrationEndDate
            // 
            this.DateEdit_CurrentRegstrationEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEdit_CurrentRegstrationEndDate.EditValue = new System.DateTime(2018, 5, 18, 0, 0, 0, 0);
            this.DateEdit_CurrentRegstrationEndDate.Location = new System.Drawing.Point(13, 399);
            this.DateEdit_CurrentRegstrationEndDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DateEdit_CurrentRegstrationEndDate.Name = "DateEdit_CurrentRegstrationEndDate";
            this.DateEdit_CurrentRegstrationEndDate.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DateEdit_CurrentRegstrationEndDate.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.DateEdit_CurrentRegstrationEndDate.Properties.Appearance.Options.UseBackColor = true;
            this.DateEdit_CurrentRegstrationEndDate.Properties.Appearance.Options.UseBorderColor = true;
            this.DateEdit_CurrentRegstrationEndDate.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.DateEdit_CurrentRegstrationEndDate.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.DateEdit_CurrentRegstrationEndDate.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.DateEdit_CurrentRegstrationEndDate.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.DateEdit_CurrentRegstrationEndDate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.DateEdit_CurrentRegstrationEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_CurrentRegstrationEndDate.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.DateEdit_CurrentRegstrationEndDate.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.DateEdit_CurrentRegstrationEndDate.Size = new System.Drawing.Size(173, 22);
            this.DateEdit_CurrentRegstrationEndDate.TabIndex = 7;
            // 
            // labelControl_TireSize
            // 
            this.labelControl_TireSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_TireSize.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl_TireSize.Appearance.Options.UseForeColor = true;
            this.labelControl_TireSize.Location = new System.Drawing.Point(230, 185);
            this.labelControl_TireSize.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl_TireSize.Name = "labelControl_TireSize";
            this.labelControl_TireSize.Size = new System.Drawing.Size(79, 17);
            this.labelControl_TireSize.TabIndex = 1000;
            this.labelControl_TireSize.Text = "حجم الكاوتش";
            // 
            // TextEdit_TireSize
            // 
            this.TextEdit_TireSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_TireSize.Location = new System.Drawing.Point(13, 181);
            this.TextEdit_TireSize.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TextEdit_TireSize.Name = "TextEdit_TireSize";
            this.TextEdit_TireSize.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TextEdit_TireSize.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.TextEdit_TireSize.Properties.Appearance.Options.UseBackColor = true;
            this.TextEdit_TireSize.Properties.Appearance.Options.UseBorderColor = true;
            this.TextEdit_TireSize.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.TextEdit_TireSize.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.TextEdit_TireSize.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.TextEdit_TireSize.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.TextEdit_TireSize.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.TextEdit_TireSize.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.TextEdit_TireSize.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.TextEdit_TireSize.Size = new System.Drawing.Size(173, 22);
            this.TextEdit_TireSize.TabIndex = 7;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Traditional Arabic", 16F, System.Drawing.FontStyle.Bold);
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.Location = new System.Drawing.Point(114, 20);
            this.simpleButton1.LookAndFeel.SkinName = "Foggy";
            this.simpleButton1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(92, 43);
            this.simpleButton1.TabIndex = 1003;
            this.simpleButton1.Text = "جديد";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Save.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Save.Appearance.Font = new System.Drawing.Font("Traditional Arabic", 16F, System.Drawing.FontStyle.Bold);
            this.btn_Save.Appearance.ForeColor = System.Drawing.Color.Green;
            this.btn_Save.Appearance.Options.UseBackColor = true;
            this.btn_Save.Appearance.Options.UseFont = true;
            this.btn_Save.Appearance.Options.UseForeColor = true;
            this.btn_Save.Location = new System.Drawing.Point(13, 20);
            this.btn_Save.LookAndFeel.SkinName = "Foggy";
            this.btn_Save.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btn_Save.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(92, 43);
            this.btn_Save.TabIndex = 1004;
            this.btn_Save.Text = "حفظ";
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Traditional Arabic", 16F, System.Drawing.FontStyle.Bold);
            this.simpleButton2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Appearance.Options.UseForeColor = true;
            this.simpleButton2.Location = new System.Drawing.Point(222, 20);
            this.simpleButton2.LookAndFeel.SkinName = "Foggy";
            this.simpleButton2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(92, 43);
            this.simpleButton2.TabIndex = 1003;
            this.simpleButton2.Text = "حذف";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.simpleButton2);
            this.groupBox1.Controls.Add(this.simpleButton1);
            this.groupBox1.Controls.Add(this.btn_Save);
            this.groupBox1.Location = new System.Drawing.Point(475, 448);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(323, 75);
            this.groupBox1.TabIndex = 1005;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Controls.Add(this.labelControl_PlateNB);
            this.groupBox2.Controls.Add(this.TextEdit_PlateNB);
            this.groupBox2.Controls.Add(this.LookUpEdit_Driver);
            this.groupBox2.Controls.Add(this.labelControl_CargoCap);
            this.groupBox2.Controls.Add(this.SpinEdit_CargoCap);
            this.groupBox2.Controls.Add(this.labelControl_TankCap);
            this.groupBox2.Controls.Add(this.SpinEdit_TankCap);
            this.groupBox2.Controls.Add(this.labelControl_CarColor);
            this.groupBox2.Controls.Add(this.TextEdit_CarColor);
            this.groupBox2.Controls.Add(this.labelControl_EngSerial);
            this.groupBox2.Controls.Add(this.TextEdit_EngSerial);
            this.groupBox2.Controls.Add(this.labelControl_ChassisNumber);
            this.groupBox2.Controls.Add(this.TextEdit_ChassisNumber);
            this.groupBox2.Controls.Add(this.labelControl_ModelYear);
            this.groupBox2.Controls.Add(this.DateEdit_ModelYear);
            this.groupBox2.Controls.Add(this.labelControl_ModelNumber);
            this.groupBox2.Controls.Add(this.TextEdit_ModelNumber);
            this.groupBox2.Controls.Add(this.labelControl_CurrentRegstrationStartDate);
            this.groupBox2.Controls.Add(this.DateEdit_CurrentRegstrationStartDate);
            this.groupBox2.Controls.Add(this.labelControl_CurrentRegstrationEndDate);
            this.groupBox2.Controls.Add(this.DateEdit_CurrentRegstrationEndDate);
            this.groupBox2.Controls.Add(this.labelControl_TireSize);
            this.groupBox2.Controls.Add(this.TextEdit_TireSize);
            this.groupBox2.Location = new System.Drawing.Point(475, 10);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(323, 431);
            this.groupBox2.TabIndex = 1006;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.gridControl1);
            this.groupBox3.Location = new System.Drawing.Point(13, 10);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(454, 513);
            this.groupBox3.TabIndex = 1007;
            this.groupBox3.TabStop = false;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Location = new System.Drawing.Point(3, 21);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(448, 488);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(264, 85);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(42, 17);
            this.labelControl1.TabIndex = 1002;
            this.labelControl1.Text = "السائق";
            // 
            // LookUpEdit_Driver
            // 
            this.LookUpEdit_Driver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_Driver.Location = new System.Drawing.Point(13, 82);
            this.LookUpEdit_Driver.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LookUpEdit_Driver.Name = "LookUpEdit_Driver";
            this.LookUpEdit_Driver.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LookUpEdit_Driver.Properties.Appearance.BorderColor = System.Drawing.Color.Silver;
            this.LookUpEdit_Driver.Properties.Appearance.Options.UseBackColor = true;
            this.LookUpEdit_Driver.Properties.Appearance.Options.UseBorderColor = true;
            this.LookUpEdit_Driver.Properties.AppearanceFocused.BackColor = System.Drawing.SystemColors.Control;
            this.LookUpEdit_Driver.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Gray;
            this.LookUpEdit_Driver.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LookUpEdit_Driver.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.LookUpEdit_Driver.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.LookUpEdit_Driver.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Show", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_Driver.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LookUpEdit_Driver.Properties.LookAndFeel.SkinName = "Office 2016 Dark";
            this.LookUpEdit_Driver.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookUpEdit_Driver.Properties.NullText = "";
            this.LookUpEdit_Driver.Size = new System.Drawing.Size(173, 22);
            this.LookUpEdit_Driver.TabIndex = 1001;
            // 
            // frm_Car
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 544);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_Car";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "السيارات";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Car_FormClosing);
            this.Load += new System.EventHandler(this.frm_Car_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_PlateNB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_CargoCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_TankCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_CarColor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_EngSerial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ChassisNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_ModelYear.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_ModelYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ModelNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_CurrentRegstrationStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_CurrentRegstrationStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_CurrentRegstrationEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_CurrentRegstrationEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_TireSize.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Driver.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.TextEdit TextEdit_PlateNB;
        DevExpress.XtraEditors.LabelControl labelControl_PlateNB;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_CargoCap;
        DevExpress.XtraEditors.LabelControl labelControl_CargoCap;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_TankCap;
        DevExpress.XtraEditors.LabelControl labelControl_TankCap;
        private DevExpress.XtraEditors.TextEdit TextEdit_CarColor;
        DevExpress.XtraEditors.LabelControl labelControl_CarColor;
        private DevExpress.XtraEditors.TextEdit TextEdit_EngSerial;
        DevExpress.XtraEditors.LabelControl labelControl_EngSerial;
        private DevExpress.XtraEditors.TextEdit TextEdit_ChassisNumber;
        DevExpress.XtraEditors.LabelControl labelControl_ChassisNumber;
        private DevExpress.XtraEditors.DateEdit DateEdit_ModelYear;
        DevExpress.XtraEditors.LabelControl labelControl_ModelYear;
        private DevExpress.XtraEditors.TextEdit TextEdit_ModelNumber;
        DevExpress.XtraEditors.LabelControl labelControl_ModelNumber;
        private DevExpress.XtraEditors.DateEdit DateEdit_CurrentRegstrationStartDate;
        DevExpress.XtraEditors.LabelControl labelControl_CurrentRegstrationStartDate;
        private DevExpress.XtraEditors.DateEdit DateEdit_CurrentRegstrationEndDate;
        DevExpress.XtraEditors.LabelControl labelControl_CurrentRegstrationEndDate;
        private DevExpress.XtraEditors.TextEdit TextEdit_TireSize;
        DevExpress.XtraEditors.LabelControl labelControl_TireSize;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btn_Save;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Driver;
    }
}