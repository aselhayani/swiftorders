﻿namespace SwiftOrders.Forms
{
    partial class frm_NewInvoices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lookUpEdit_Store = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_Drawer = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.btn_Convert = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Store.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Drawer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lookUpEdit_Store
            // 
            this.lookUpEdit_Store.Location = new System.Drawing.Point(12, 12);
            this.lookUpEdit_Store.Name = "lookUpEdit_Store";
            this.lookUpEdit_Store.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_Store.Properties.NullText = "المخزن";
            this.lookUpEdit_Store.Size = new System.Drawing.Size(296, 20);
            this.lookUpEdit_Store.TabIndex = 0;
            // 
            // lookUpEdit_Drawer
            // 
            this.lookUpEdit_Drawer.Location = new System.Drawing.Point(12, 38);
            this.lookUpEdit_Drawer.Name = "lookUpEdit_Drawer";
            this.lookUpEdit_Drawer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_Drawer.Properties.NullText = "الخزينه";
            this.lookUpEdit_Drawer.Size = new System.Drawing.Size(296, 20);
            this.lookUpEdit_Drawer.TabIndex = 0;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(13, 65);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.NullText = "السائق";
            this.textEdit1.Size = new System.Drawing.Size(295, 20);
            this.textEdit1.TabIndex = 1;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(13, 91);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.NullText = "الوجهه";
            this.textEdit2.Size = new System.Drawing.Size(295, 20);
            this.textEdit2.TabIndex = 1;
            // 
            // btn_Convert
            // 
            this.btn_Convert.Location = new System.Drawing.Point(233, 130);
            this.btn_Convert.Name = "btn_Convert";
            this.btn_Convert.Size = new System.Drawing.Size(75, 23);
            this.btn_Convert.TabIndex = 2;
            this.btn_Convert.Text = "انشاء";
            this.btn_Convert.Click += new System.EventHandler(this.btn_Convert_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(13, 130);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel.TabIndex = 2;
            this.btn_Cancel.Text = "الغاء";
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEdit1.Location = new System.Drawing.Point(12, 174);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(296, 0);
            this.memoEdit1.TabIndex = 3;
            // 
            // frm_NewInvoices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 163);
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Convert);
            this.Controls.Add(this.textEdit2);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.lookUpEdit_Drawer);
            this.Controls.Add(this.lookUpEdit_Store);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_NewInvoices";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "انشاء فواتير مبيعات ";
            this.Load += new System.EventHandler(this.frm_NewInvoices_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Store.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Drawer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_Store;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_Drawer;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        public DevExpress.XtraEditors.SimpleButton btn_Convert;
        private DevExpress.XtraEditors.SimpleButton btn_Cancel;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
    }
}