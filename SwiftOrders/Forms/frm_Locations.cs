﻿using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_Locations : DevExpress.XtraEditors.XtraForm
    {
        public frm_Locations()
        {
            InitializeComponent();
        }
        DAL.Distnation distnation = new DAL.Distnation();

        private void frm_Locations_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = DAL.Distnation.ToData();
            gridView1.Columns["ID"].Visible = false;
            IntGridControl2();
        }
        void IntGridControl2()
        {
            gridView2.Columns["ID"].Visible = false;
            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            DAL.Distnation.ToRepoLookUp(repo);
            gridControl2.RepositoryItems.Add(repo);
            gridView2.Columns["Point"].ColumnEdit = repo;
            gridView2.Columns["Point"].OptionsColumn.AllowEdit = false; 
            RepositoryItemSpinEdit spinEdit = new RepositoryItemSpinEdit();
            gridControl2.RepositoryItems.Add(spinEdit);
            gridView2.Columns["Distance"].ColumnEdit = spinEdit;

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textEdit1.Text)) return;
            simpleButton1.Enabled = false;
            distnation = new DAL.Distnation();
            distnation.Name = textEdit1.Text;
            distnation.Save();
            gridControl1.DataSource = DAL.Distnation.ToData();
            gridView1.Columns["ID"].Visible = false;
            textEdit1.Text = "";
            gridControl2.DataSource = distnation.DistancesDT();
            IntGridControl2();


            simpleButton1.Enabled = true ;
        }

        private void gridView2_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
           if( e.Column.FieldName == "Distance" && Convert .ToInt32( e.Value ) > 0)
            {
                Master.SQlCmd("Update Distances Set Distance = '" + e.Value + "' where ID = '" + gridView2.GetRowCellValue(e.RowHandle, "ID") + "'");
            }
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            distnation = new DAL.Distnation(Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID")));
            gridControl2.DataSource = distnation.DistancesDT();
            IntGridControl2();

        }

        private void حذفToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle > 0 && Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID"))> 0)
            {
                if(Master .ShowDeleteConfrmationDialog(this, "هل تريد حذف الواجه ؟ "))
                {
                    distnation = new DAL.Distnation(Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID")));
                    distnation.Delete();

                }
            }
        }
    }
}
