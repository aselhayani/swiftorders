﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_NewInvoices : DevExpress.XtraEditors.XtraForm
    {
    
        List<DAL.Order > orders = new List<DAL.Order>();

        public frm_NewInvoices(List<int> OrdersIDs)
        {
            InitializeComponent();
            foreach (int  id in OrdersIDs) orders.Add(new DAL.Order(id));

        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btn_Convert_Click(object sender, EventArgs e)
        {
           
            this.ClientSize = new System.Drawing.Size(320, 381);

            int count = 0;

            foreach (DAL.Order  order in orders )
            {
                memoEdit1.Text += Environment.NewLine;
               memoEdit1.Text += " جاري انشاء فاتوره للطلبيه رقم<<<   " + order.ID + Environment.NewLine;
                if (!(order.InvoiceCode.Length > 1))
                {
                    DAL.Invoice invoice = new DAL.Invoice(order);
                    invoice.StoreId = (int)lookUpEdit_Store.EditValue;
                    invoice.DrawerAccountId = (int)lookUpEdit_Drawer.EditValue;
                    invoice.Destination = textEdit2.Text;
                    invoice.DriverName = textEdit1.Text;
                    invoice.Save();
                    memoEdit1.Text += " تم انشاء الفاتوره )  " + invoice.InvoiceCode + "( بنجاح " + Environment.NewLine ;

                }
                else
                {
                    memoEdit1.Text +=" فشل انشاء الفاتوره  "+ Environment.NewLine;
                    memoEdit1.Text += "يوجد فاتوره لهذه الطلبيه بالفعل ) فاتوره رقم " +  order.InvoiceCode +  ") " + Environment.NewLine;
                    count++;
                }
                Refresh();
            }


            btn_Convert.Enabled = false;
            btn_Cancel .Text  = " انهاء ";

        }

        private void frm_NewInvoices_Load(object sender, EventArgs e)
        {

            LoadData();


        }
        public void LoadData()
        {
            lookUpEdit_Store.Properties.DataSource = Master.SqlDataTable(" use erp SELECT  StoreId  , StoreNameAr    FROM  IC_Store ");
            lookUpEdit_Drawer.Properties.DataSource = Master.SqlDataTable("  use erp SELECT DrawerNameAr as 'Drawer' ,AccountId FROM ACC_Drawer union  SELECT  BankName as 'Drawer' ,AccountId FROM ACC_Bank");

            lookUpEdit_Drawer.Properties.ValueMember = "AccountId";
            lookUpEdit_Drawer.Properties.DisplayMember = "Drawer";

            lookUpEdit_Store.Properties.ValueMember = "StoreId";
            lookUpEdit_Store.Properties.DisplayMember = "StoreNameAr";

            lookUpEdit_Store.Properties.PopulateColumns();
            lookUpEdit_Drawer.Properties.PopulateColumns();


            //lookUpEdit_Store.Properties.Columns[1].Caption = "المخزن";
            //lookUpEdit_Store.Properties.Columns[0].Visible = false;

            //lookUpEdit_Drawer.Properties.Columns[1].Caption = "الخذينه";
            //lookUpEdit_Drawer.Properties.Columns[0].Visible = false;

            lookUpEdit_Drawer.EditValue = 9;
            lookUpEdit_Store.EditValue = 6;
        }
    }
}
