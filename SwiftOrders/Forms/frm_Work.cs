﻿using DevExpress.Data;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_Work : DevExpress.XtraEditors.XtraForm
    {
        DataTable ShipmentOrdersTable = new DataTable();
        DataTable ShipmentProducTable = new DataTable();

        public frm_Work()
        {
            InitializeComponent();
        }
        void GetData()
        {
            ShipmentProducTable = Master.SqlDataTable("SELECT [Product],[Qty],[ExtraQty] ,(t.Qty + t.ExtraQty)as TotalQty FROM  [ShipProducts] as t where [ShipID] = 0");
            gridControl4.DataSource = ShipmentProducTable;

            for (int i = 1; i < gridView4.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView4.Columns[i].Summary.Clear();
                gridView4.Columns[i].Summary.Add(siTotal);
            }
            //gridView4.Columns[0].OptionsColumn.AllowEdit = false;
            gridView4.Columns[1].OptionsColumn.AllowEdit = false;
            gridView4.Columns[3].OptionsColumn.AllowEdit = false;

            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            RepositoryItemSpinEdit repoSpinEdit = new RepositoryItemSpinEdit();
            DAL.Item.ToRepoLookUp(repo);
            gridControl4.RepositoryItems.Add(repo);
            gridControl4.RepositoryItems.Add(repoSpinEdit);
            gridView4.Columns["Product"].ColumnEdit = repo;
            gridView4.Columns["ExtraQty"].ColumnEdit = repoSpinEdit;
            gridView4.Columns["ExtraQty"].Caption = "الكميه الاضافيه";
            gridView4.Columns["Product"].Caption = "المنتج";
            gridView4.Columns["Qty"].Caption = "الكميه من الطلبيات";
            gridView4.Columns["TotalQty"].Caption = "اجمالي الكميه";

            string products = "  ";
            foreach (DataRow row in DAL.Item.ToRepoDataTable().Rows) products += "  ,IsNull( (SELECT SUM( [Qty]) FROM [OMDB].[dbo].[OrderProducts] where  [OrderID] = t.ID and [Product] = " + row["ItemId"].ToString() + " ) , 0) as ' " + row["ItemNameAr"].ToString() + "'  ";
            ShipmentOrdersTable = Master.SqlDataTable("SELECT    "
               + "      [ID] "
               + "     ,[Clint]                                                 "
               + "     ,[Date]                                                  "
               + "     ,[DeliveryDate]                                          "
               + "                                           "
               + "                                                        "
               + "     ,[Weight]                                                "
               + "                                                "
               + "     ,[OrderGroup]                                            "
               + "     ,[TotalPrice]                                            "
               + "     ,OrderSort                                 "
               + products
               + " FROM[OMDB].[dbo].[Order] as t Where    "
               + "  DeliveryDate < '"+DateTime.Now.ToString("yyyy-MM-dd")+ "' and Status  in (2,1)   ");
            gridControl1.DataSource = ShipmentOrdersTable;

            gridView1.Columns["ID"].Caption = "كود";
            gridView1.Columns["Clint"].Caption = "العميل";
            gridView1.Columns["Date"].Caption = "تاريخ الطلب";
            gridView1.Columns["Date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridView1.Columns["Date"].DisplayFormat.FormatString = "yyyy-MM-dd hh:mm tt";
            gridView1.Columns["DeliveryDate"].Caption = "موعد التسليم";
            gridView1.Columns["Weight"].Caption = "الوزن";
            gridView1.Columns["OrderGroup"].Caption = "المجموعه";
            gridView1.Columns["TotalPrice"].Caption = "قمه الطلبيه";


            for (int i = 8; i < gridView1.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView1.Columns[i].Summary.Clear();
                gridView1.Columns[i].Summary.Add(siTotal);
                var total = ShipmentOrdersTable.Compute("SUM([" + ShipmentOrdersTable.Columns[i].ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) == 0)
                {
                    gridView1.Columns[i].Visible = false;
                }
                else gridView1.Columns[i].Visible = true;
            }
            for (int i = 1; i < gridView1.Columns.Count; i++)
            {
                gridView1.Columns[i].OptionsColumn.AllowEdit = false;

            }

            gridView1.Columns["ID"].Fixed = FixedStyle.Left;
            gridView1.Columns["Clint"].Fixed = FixedStyle.Left;
            gridView1.Columns["Date"].Fixed = FixedStyle.Left;
            //gridView1.Columns["Status"].Fixed = FixedStyle.Right;

            gridView1.Columns["Clint"].Width = 150;
            RepositoryItemLookUpEdit RepositoryOrderState = new RepositoryItemLookUpEdit();
            RepositoryOrderState.DataSource = frm_Orders.OrderState();
            RepositoryOrderState.ValueMember = "ID";
            RepositoryOrderState.DisplayMember = "Name";
            RepositoryItemLookUpEdit RepositoryClint = new RepositoryItemLookUpEdit();
            RepositoryClint.DataSource = Master.ClintsTable;
            RepositoryClint.ValueMember = "Customerid";
            RepositoryClint.DisplayMember = "CusNameAr";
            RepositoryItemLookUpEdit RepositoryCar = new RepositoryItemLookUpEdit();
            RepositoryCar.DataSource = DAL.Car.ToData(false);
            RepositoryCar.ValueMember = "ID";
            RepositoryCar.DisplayMember = "PlateNB";
            RepositoryItemLookUpEdit RepositoryGroup = new RepositoryItemLookUpEdit();
            RepositoryGroup.DataSource = frm_Orders.OrderGroup();
            RepositoryGroup.ValueMember = "ID";
            RepositoryGroup.DisplayMember = "Name";

            RepositoryItemLookUpEdit RepositoryOrdersEdit = new RepositoryItemLookUpEdit();
            RepositoryOrdersEdit.DataSource = Master.SqlDataTable("Select ID , CusNameAr as 'Clint' ,Weight ,Date  FROM ( [Order] as t join ERP.dbo.SL_Customer  on Customerid = Clint )  ");
            RepositoryOrdersEdit.ValueMember = "ID";
            RepositoryOrdersEdit.DisplayMember = "ID";
            gridControl1.RepositoryItems.Add(RepositoryOrdersEdit);
            gridView1.Columns["ID"].ColumnEdit = RepositoryOrdersEdit;



            gridControl1.RepositoryItems.AddRange(new RepositoryItem[] { RepositoryOrderState, RepositoryClint, RepositoryCar, RepositoryGroup });

            gridView1.Columns["Clint"].ColumnEdit = RepositoryClint;

            gridView1.Columns["OrderGroup"].ColumnEdit = RepositoryGroup;
            gridView1.Columns["OrderSort"].Visible = false;
            gridView1.ClearSorting();
            gridView1.Columns["OrderSort"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;


            //gridView1.Columns["OrderSort"].Visible = false;
        }
        private void gridView1_DataSourceChanged(object sender, EventArgs e)
        {
            ShipmentProducTable.AcceptChanges();
            foreach (DataRow row in ShipmentProducTable.Rows)
            {
                row["Qty"] = 0;
            }
            for (int i = 8; i < ShipmentOrdersTable.Columns.Count; i++)
            {
                DataColumn clm = ShipmentOrdersTable.Columns[i];
                var total = ShipmentOrdersTable.Compute("SUM([" + clm.ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) > 0)
                {
                    // Get column product id 
                    int itemid = Convert.ToInt32(Master.SqlDataTable(" SELECT [ItemId]  FROM [ERP].[dbo].[IC_Item] where   ItemNameAr = N'" + clm.ColumnName.TrimStart() + "'  ").Rows[0][0]);
                    List<DataRow> row = ShipmentProducTable.Select("Product = " + itemid + "").ToList<DataRow>();
                    if (row.Count == 0)
                    {
                        ShipmentProducTable.Rows.Add();
                        int last = ShipmentProducTable.Rows.Count - 1;
                        ShipmentProducTable.Rows[last]["Product"] = itemid;
                        ShipmentProducTable.Rows[last]["Qty"] = total;
                    }
                    else if (row.Count == 1)
                    {
                        row[0]["Qty"] = total;
                    }
                    gridControl4.DataSource = ShipmentProducTable;
                }
            }

            CalculatTotalProductsColumn();

        }
        Boolean SusspendCalculation = false;
        private void gridView4_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            SusspendCalculation = true;
            GridView view = sender as GridView;
            view.SetRowCellValue(e.RowHandle, "ExtraQty", Convert.ToDouble(0));
            view.SetRowCellValue(e.RowHandle, "Qty", Convert.ToDouble(0));
            SusspendCalculation = false;

        }
        private void gridView4_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == "Product")
            {

                string IDs = "0";
                for (int i = 0; i < gridView4.RowCount; i++)
                {
                    IDs += "," + "'" + gridView4.GetRowCellValue(i, "Product") + "'";
                }
                RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
                repo.DataSource = Master.SqlDataTable(" SELECT [ItemId] ,[ItemNameAr] FROM [ERP].[dbo].[IC_Item] where  itemtype = 2 and IsDeleted = 0 and ItemId not in (" + IDs + ")  ");
                repo.ValueMember = "ItemId";
                repo.DisplayMember = "ItemNameAr";
                repo.PopulateColumns();
                repo.Columns["ItemId"].Visible = false;
                e.RepositoryItem = repo;

            }

        }
        private void gridView4_FocusedRowObjectChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowObjectChangedEventArgs e)
        {
            if (gridView4.GetFocusedRowCellValue("Qty") != null && gridView4.GetFocusedRowCellValue("Qty") != DBNull.Value && (double)gridView4.GetFocusedRowCellValue("Qty") > 0)
            {
                gridView4.Columns["Product"].OptionsColumn.AllowEdit = false;

            }
            else
            {
                gridView4.Columns["Product"].OptionsColumn.AllowEdit = true;

            }
        }
        void CalculatTotalProductsColumn()
        {
            if (SusspendCalculation) return;
            double TotalWieght = 0;
            ShipmentProducTable.AcceptChanges();
            foreach (DataRow row in ShipmentProducTable.Rows)
            {

                if (row["Qty"] == null || row["Qty"] == DBNull.Value) row["Qty"] = 0;
                if (row["ExtraQty"] == null || row["ExtraQty"] == DBNull.Value) row["ExtraQty"] = 0;
                row["TotalQty"] = (Double)row["ExtraQty"] + (Double)row["Qty"];
                TotalWieght += (double)row["TotalQty"] * new DAL.Item((int)row["Product"]).MediumUOMFactor;
            }
            ShipmentProducTable.AcceptChanges();
            foreach (DataRow row in ShipmentProducTable.Rows)
            {
                if (row["Qty"].GetType() == typeof(double) && row["ExtraQty"].GetType() == typeof(double) && (Double)row["ExtraQty"] + (Double)row["Qty"] == 0)
                    row.Delete();
            }
            ShipmentProducTable.AcceptChanges();
           
            gridControl4.DataSource = ShipmentProducTable;
        }
        private void gridView4_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "Qty" || e.Column.FieldName == "ExtraQty")
            {
                CalculatTotalProductsColumn();
            }
        }
        private void gridControl4_ProcessGridKey(object sender, KeyEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete && view.FocusedRowHandle > 0)
            {

                if (view.GetFocusedRowCellValue("Qty") != null && view.GetFocusedRowCellValue("Qty") != DBNull.Value && (double)view.GetFocusedRowCellValue("Qty") == 0)
                {
                    view.DeleteSelectedRows();
                    e.Handled = true;
                }

            }
        }
        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete && view.FocusedRowHandle >= 0)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
                ShipmentOrdersTable.AcceptChanges();
                string IDs = "0";
                for (int i = 0; i < ShipmentOrdersTable.Rows.Count; i++)
                {
                    IDs += "," + "'" + ShipmentOrdersTable.Rows[i]["ID"] + "'";
                }

                GetCurrentOrders(IDs);
            }
        }
        void GetCurrentOrders(string ids = "")
        {
            string products = "  ";
            foreach (DataRow row in DAL.Item.ToRepoDataTable().Rows) products += "  ,IsNull( (SELECT SUM( [Qty]) FROM [OMDB].[dbo].[OrderProducts] where  [OrderID] = t.ID and [Product] = " + row["ItemId"].ToString() + " ) , 0) as ' " + row["ItemNameAr"].ToString() + "'  ";

            string IDs = ids;

            ShipmentOrdersTable = Master.SqlDataTable("SELECT    "
                + "      [ID] "
                + "     ,[Clint]                                                 "
                + "     ,[Date]                                                  "
                + "     ,[DeliveryDate]                                          "
                + "                                           "
                + "                                                        "
                + "     ,[Weight]                                                "
                + "                                                "
                + "     ,[OrderGroup]                                            "
                + "     ,[TotalPrice]                                            "
                + "     ,OrderSort                                 "
                + products
                + " FROM[OMDB].[dbo].[Order] as t Where    "
                + "    ID in (" + IDs + ") Order by OrderSort ");

            gridControl1.DataSource = ShipmentOrdersTable;

            for (int i = 8; i < gridView1.Columns.Count; i++)
            {
                GridColumnSummaryItem siTotal = new GridColumnSummaryItem();
                siTotal.SummaryType = SummaryItemType.Sum;
                siTotal.DisplayFormat = "{0}";
                gridView1.Columns[i].Summary.Clear();
                gridView1.Columns[i].Summary.Add(siTotal);
                var total = ShipmentOrdersTable.Compute("SUM([" + ShipmentOrdersTable.Columns[i].ColumnName + "])", string.Empty);
                if (total != DBNull.Value && Convert.ToInt32(total) == 0)
                {
                    gridView1.Columns[i].Visible = false;
                }
                else gridView1.Columns[i].Visible = true;
            }
            gridView1.Columns["OrderSort"].Visible = false;
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DataTable ProdctTableForPrint = new DataTable();
            ProdctTableForPrint.Columns.Add("Product");
            ProdctTableForPrint.Columns.Add("Qty");


            for (int i = 0; i < gridView4.RowCount - 1; i++)
            {
                ProdctTableForPrint.Rows.Add();
                ProdctTableForPrint.Rows[i][0] = gridView4.GetRowCellDisplayText(i, "Product");
                ProdctTableForPrint.Rows[i][1] = gridView4.GetRowCellValue(i, "TotalQty");
            }


            Reporting.rpt_Work  report = new Reporting.rpt_Work(ProdctTableForPrint );
            report.LoadData();

            using (ReportPrintTool printtool = new ReportPrintTool(report))
            {
                printtool.ShowRibbonPreviewDialog();
            }
        }
        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "ID" && e.Value.GetType() == typeof(int))
            {
                 
                string IDs = "0,'" + e.Value + "'";

                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    IDs += "," + "'" + gridView1.GetRowCellValue(i, "ID") + "'";
                }

                GetCurrentOrders(IDs);



            }
        }
        private void gridView1_CustomRowCellEditForEditing(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == "ID")
            {
                string IDs = "";
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    IDs += "," + "'" + gridView1.GetRowCellValue(i, "ID") + "'";
                }

                RepositoryItemLookUpEdit CustomRepositoryEdit = new RepositoryItemLookUpEdit();
                CustomRepositoryEdit.DataSource = Master.SqlDataTable("Select ID , CusNameAr as 'Clint' ,Weight ,Date  FROM ( [Order] as t join ERP.dbo.SL_Customer  on Customerid = Clint )"
                + " where  Status in( 2,1)                                "
                + " and ID not in (Select OrderID from ShipOrders)   "
                + " and ID not in (0" + IDs + ") Order by OrderSort ");
                CustomRepositoryEdit.ValueMember = "ID";
                CustomRepositoryEdit.DisplayMember = "ID";
                gridControl1.RepositoryItems.Add(CustomRepositoryEdit);
                e.RepositoryItem = CustomRepositoryEdit;
            }
        }
        private void frm_Work_Load(object sender, EventArgs e)
        {
            gridView1.DataSourceChanged += gridView1_DataSourceChanged;
            gridView1.CustomRowCellEditForEditing += gridView1_CustomRowCellEditForEditing;
            gridView4.InitNewRow += gridView4_InitNewRow;
            gridView4.CustomRowCellEditForEditing += gridView4_CustomRowCellEditForEditing;
            gridView4.FocusedRowObjectChanged += gridView4_FocusedRowObjectChanged;
            gridView4.CellValueChanged += gridView4_CellValueChanged;
            gridControl4.ProcessGridKey += gridControl4_ProcessGridKey;
            gridControl1.ProcessGridKey += gridControl1_ProcessGridKey;
            simpleButton1.Click += simpleButton1_Click;
            gridView1.CellValueChanged += gridView1_CellValueChanged;
            GetData();
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
