﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SwiftOrders.Forms
{
    public partial class frm_Main : XtraForm
    {
        public frm_Main()
        {
            InitializeComponent();
        }
        public   void OpenForm(Form frm)
        {
            frm.MdiParent = this;
            frm.Opacity = 0;
            frm.Show();
        }
      
        private void طلبيهجديدهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(new frm_NewOrder());

        }

     

        private void اضافهتعديلToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(new frm_Car());
        }

        private void frm_Main_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        private void frm_Main_KeyDown(object sender, KeyEventArgs e)
        {
          
            if (e.Modifiers == Keys.Alt)
            {
                switch (e.KeyCode)
                {
                    case Keys.N: OpenForm(new frm_NewOrder()); break;
                    case Keys.A: OpenForm(new frm_Orders()); break;
                    case Keys.R: OpenForm(new frm_Orders(0, false)); break;
                    case Keys.S: OpenForm(new frm_Orders(1,false )); break;
                    case Keys.D: OpenForm(new frm_Orders(2, false)); break;
                }
            }
        }

        private void الكلToolStripMenuItem_Click(object sender, EventArgs e)
        {
             OpenForm(new frm_Orders()); 
        }

        private void بانتظارالمراجعهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(new frm_Orders(0,false));
        }

        private void بانتظرتبليغالعميلToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(new frm_Orders(1, false));
        }

        private void بانتظارالتسليمToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(new frm_Orders(2, false));
        }

        private void تمتسليمهاToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(new frm_Orders(3, false));
        }

        private void frm_Main_Load(object sender, EventArgs e)
        {
            Master.GetProductsDate();
            Master.GetClintsDate();
            CalculateNotficationPanal();
           
        }

        private void frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Form frm in this.MdiChildren )    frm.Close();
            Application.Exit();
            
           
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DataTable NotfyDT = new DataTable();
            string   tipTitle = "";
            string   tipText = "";


            NotfyDT = Master.SqlDataTable("SELECT TOP 1 [ID],[NotfyTime]  ,[Type],[TypeID] FROM  [Notfications]  order by [ID] desc ");
            if (NotfyDT.Rows.Count == 0) return;
            int lastnotfy = Properties.Settings.Default.LastNotfication;
            if (((int)NotfyDT.Rows[0]["ID"]) > lastnotfy)
            {
                       
                switch ((int)NotfyDT.Rows[0]["Type"])
                {
                    case 0:
                        DAL.Order order = new DAL.Order((int)NotfyDT.Rows[0]["TypeID"]);
                        if (order.Status == 0) tipTitle = "لقد تم اضافه طلبيه جديده وتحتاج الي المراجعة";
                        else tipTitle = "لقد تم تعديل بيانات طلبيه ";
                        tipText += " : الكود  " + order.ID + Environment.NewLine;
                        tipText += " : الحاله  " + frm_Orders.OrderState().Rows[order.Status][1]  + Environment.NewLine;
                        break;
                }
                //notifyIcon1.ShowBalloonTip(2500, tipTitle, tipText, ToolTipIcon.Info);
                alertControl1.Show(owner: this, caption: "لقد تم تعديل بيانات طلبيه ", text: "يوجد طلبيه تحتاج الي المراجعه", hotTrackedText: "", tag: NotfyDT.Rows[0]["TypeID"], autoCloseFormOnClick: false,image :Properties.Resources.Image1);
                Properties.Settings.Default.LastNotfication = (int)NotfyDT.Rows[0]["ID"];
                Properties.Settings.Default.Save();
                CalculateNotficationPanal();
            }

        }
        public  void CalculateNotficationPanal()
        {
            TextEdit_0.Text = Master.SqlDataTable("Select COUNT([ID]) from [OMDB].[dbo].[Order] Where [Status] = 0").Rows[0][0].ToString() + " طلبيه  " ;
            TextEdit_1.Text = Master.SqlDataTable(" Select COUNT([ID]) from [OMDB].[dbo].[Order] Where [Status] = 1").Rows[0][0].ToString() + " طلبيه  ";
            textEdit_Today.Text = Master.SqlDataTable("  Select COUNT([ID])   from [OMDB].[dbo].[Order] Where [Status] = 2 and [DeliveryDate] = '" + DateTime.Today.ToString("yyyy-MM-dd ") + "' ").Rows[0][0].ToString() + " طلبيه  ";
            TextEdit_Ton .Text = Master.SqlDataTable("  Select IsNull(Sum([Weight]),0)   from [OMDB].[dbo].[Order] Where [Status] = 2 and [DeliveryDate] = '" + DateTime.Today.ToString("yyyy-MM-dd ") + "' ").Rows[0][0].ToString() + " كيلو  ";

            DataTable dt = new DataTable();
            dt = Master.SqlDataTable ("  SELECT   [Product] ,SUM([Qty])FROM  [OMDB].[dbo].[OrderProducts] where [OrderID] in (  Select  [ID]  from  [OMDB].[dbo].[Order] Where [Status] = 2 and [DeliveryDate] = '" + DateTime.Today.ToString("yyyy-MM-dd ") + "' ) group by Product ");
            gridControl1.DataSource = dt;
            RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
            DAL.Item.ToRepoLookUp(repo);
            //gridControl1.RepositoryItems.Add(repo);
            //gridView1.Columns["Product"].ColumnEdit = repo;
            //gridView1.Columns["Product"].Caption = "الصنف";
            //gridView1.Columns[1].Caption = "الكميه";

            //dt = Master.SqlDataTable("   SELECT [PlateNB],[CargoCap] , (   Select IsNull(Sum([Weight]),0)   from [OMDB].[dbo].[Order] Where [Status] = 2 and car = t.ID and [DeliveryDate] =  '" + DateTime.Today.ToString("yyyy-MM-dd ") + "' )  FROM [OMDB].[dbo].[Car]  as t  ");
            //gridControl2.DataSource = dt;

        }

        private void labelControl1_Click(object sender, EventArgs e)
        {
            بانتظارالمراجعهToolStripMenuItem.PerformClick();

        }

        private void labelControl2_Click(object sender, EventArgs e)
        {
            بانتظرتبليغالعميلToolStripMenuItem.PerformClick();

        }

        private void labelControl3_Click(object sender, EventArgs e)
        {
            بانتظارالتسليمToolStripMenuItem.PerformClick();
        }

        private void مأموريهجديدهToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void تحديثToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CalculateNotficationPanal();
        }

        private void الواجهاتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_Locations().ShowDialog();
        }

        private void مسحوباتالعملاءToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_CustomerReport().ShowDialog();

        }

        private void تسجيلقراءهعدادToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_LogCarRecord().ShowDialog();

        }

        private void alertControl1_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
        {
            new frm_EditOrder((int)e.Info.Tag).Show();
        }

        private void طباعهامرتشغيلToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_Work().ShowDialog();

        }

        private void السائقونToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_Drivers().ShowDialog();
        }

        private void جديدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_Shipment().ShowDialog();
        }

        private void عرضToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenForm(   new frm_AllShipments());
        }

        private void اعادهتصميمالتقاريرToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}












