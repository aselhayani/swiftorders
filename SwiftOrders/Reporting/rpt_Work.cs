﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Globalization;

namespace SwiftOrders.Reporting
{
    public partial class rpt_Work : DevExpress.XtraReports.UI.XtraReport
    {
        DataTable ProducT = new DataTable();
        string date = "yyyy-MM-dd";
       
        public rpt_Work(DataTable ProducT)
        {
            InitializeComponent();
            this.ProducT = ProducT;
            this.DataSource = this.ProducT;
            DateTime time = DateTime.Now;
            CultureInfo ci = new CultureInfo("AR-EG");
            this.lbl_Date.Text = time .ToString("yyyy-MM-dd  dddd", ci );
        }
        public void LoadData()
        {

            this.DataSource = this.ProducT;
            this.cell_Product.DataBindings.Add("Text", this.DataSource, "Product");
            this.Cell_Qty.DataBindings.Add("Text", this.DataSource, "Qty");

        }

    }
}
