﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.DAL
{
    class Distnation
    {
        public Distnation()
        {
            IsNew = true;
            ID = Master.GetNewID("ID", "Distnations");
        }
        public Distnation(int id)
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Distnations where ID  = '" + id + "' ");
            IsNew = false;
            ID = (Int32)DA.Rows[0]["ID"];
            Name = DA.Rows[0]["Name"].ToString();
        }
        public static int GetLocalDistance(int start , int end)
        {
            int result = 0;

            DataTable table = Master.SqlDataTable("SELECT [Distance]FROM  [Distances] where [StartPoint] in(" + start + "," + end + ") and [EndPoint] in (" + start+","+end+")");
            if (table.Rows.Count > 0)
            {
                return Convert.ToInt32( table.Rows[0][0]);
            }
            else return result;

           

        }
        public Boolean IsNew { get; set; }
        public Int32 ID { get; set; }
        public string Name { get; set; }
        public void Save()
        {
            if (IsNew == true)
            {
                Master.GetNewID("ID", "Distnations");
                Master.SQlCmd("Insert Into Distnations(ID)Values( " + ID + ") ");
                CreatDistances();
            }
            string SaveSQltxt = " Update Distnations set "
             + " ID = '" + ID + "' "
             + ",Name = '" + Name + "' "
             + " from  Distnations where ID  = '" + ID + "' ";
            Master.SQlCmd(SaveSQltxt);
            IsNew = false;
        }
        public void Delete()
        {
            //if ((Int32)Master.SqlDataTable(" select COUNT( )FROM   where ID = '" + ID + "'").Rows[0][0] != 0) { Master.ShowInformMessseg(Master.MainForm, LangResorces.CantDelete, LangResorces.DeleteAbortMesg); return; }
            //if (Master.ShowDeleteConfrmationDialog(Master.MainForm, LangResorces.ConfrmDeleteStore + Name) == true)
            //{
            Master.SQlCmd(" delete from Distnations where ID  = '" + ID + "'");
            Master.SQlCmd(" delete from Distances Where [StartPoint] = '" + ID + "'  or [EndPoint] = '" + ID + "'");

            IsNew = true;
                
            //}
        }

        public static DataTable ToData()
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Distnations  ");
            return DA;
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp)
        {
            lookUp.Properties.DataSource = ToData();
            lookUp.Properties.ValueMember = "ID";
            lookUp.Properties.DisplayMember = "Name";

        }
        public static void ToRepoLookUp(DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUp)
        {
            lookUp.DataSource = ToData ();
            lookUp.ValueMember = "ID";
            lookUp.DisplayMember = "Name";
            lookUp.PopulateColumns();
            lookUp.Columns["ID"].Visible = false;
        }
        void CreatDistances()
        {
            DataTable dt = new DataTable();
            dt = Master.SqlDataTable("Select * From Distnations  where ID <> '"+ ID +"' ");
            foreach (DataRow row  in dt.Rows )
            {

                int dis = getDistance(Name, row["Name"].ToString());
                if (dis == 0)
                {
                    dis = getDistance(Name, row["Name"].ToString());
                }

                Master.SQlCmd("Insert into Distances (StartPoint,EndPoint,Distance )values ('"+ID+"','"+row["ID"]+"','"+ dis + "') ");
            }
        }
        public  DataTable DistancesDT()
        {
            return Master.SqlDataTable("SELECT [ID] ,[EndPoint] as 'Point',[Distance] FROM [Distances] where StartPoint = '" + ID + "'     "
                                       + "   union                                                                               "
                                       + " SELECT[ID],[StartPoint] as 'Point',[Distance]  FROM[Distances] where[EndPoint] = '" + ID + "'");
        }

    

    public int getDistance(string origin, string destination)
    {
            if (string.IsNullOrWhiteSpace(origin) || string.IsNullOrWhiteSpace(destination)) return 0;

            System.Threading.Thread.Sleep(500);
       
        int distance = 0;
        string url = "http://maps.googleapis.com/maps/api/directions/json?origin=" + origin + "&destination=" + destination + "&sensor=false&Key=AIzaSyAKgh2MTni_-0QdwQqO9DvMnUCOVF0rtIg";
        string requesturl = url;
        string content = fileGetContents(requesturl);
        JObject o = JObject.Parse(content);
        try
        {
            distance = (int)o.SelectToken("routes[0].legs[0].distance.value")/ 1000;
            return distance;
        }
        catch(Exception exception )
        {
              
            return distance;
        }
       
    }

    protected string fileGetContents(string fileName)
    {
        string sContents = string.Empty;
        string me = string.Empty;
        try
        {
            if (fileName.ToLower().IndexOf("http:") > -1)
            {
                System.Net.WebClient wc = new System.Net.WebClient();
                byte[] response = wc.DownloadData(fileName);
                sContents = System.Text.Encoding.ASCII.GetString(response);

            }
            else
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(fileName);
                sContents = sr.ReadToEnd();
                sr.Close();
            }
        }
        catch { sContents = "unable to connect to server "; }
        return sContents;
    }
  }
}
