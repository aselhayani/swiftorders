﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.DAL
{
    class Order
    {
        public Order()
        {
            IsNew = true;
            ID = Master.GetNewID("ID", "Order");
            OrderSort = Master.GetNewID("OrderSort", "Order");
            Date = DateTime.Now;
            Clint = 0;
            OrderGroup = 0;
            getDetails();
        }
        public Order(int id)
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From [Order] where ID  = '" + id + "' ");
            IsNew = false;
            ID = (Int32)DA.Rows[0]["ID"];
            OrderSort = (Int32)DA.Rows[0]["OrderSort"];
            Clint = (Int32)DA.Rows[0]["Clint"];
            Date = (DateTime)DA.Rows[0]["Date"];
            DeliveryDate = (DateTime)DA.Rows[0]["DeliveryDate"];
            TrueDeliveryDate = (DateTime)DA.Rows[0]["TrueDeliveryDate"];
            Car = (Int32)DA.Rows[0]["Car"];
            Driver = (Int32)DA.Rows[0]["Driver"];
            Weight = (Double)DA.Rows[0]["Weight"];
            InvoiceCode = DA.Rows[0]["InvoiceCode"].ToString();
            OrderGroup = (Int32)DA.Rows[0]["OrderGroup"];
            TotalPrice = (Double)DA.Rows[0]["TotalPrice"];
            Status = (Int32)DA.Rows[0]["Status"];
            Reported = (Boolean)DA.Rows[0]["Reported"];
            getDetails();
        }
        public Boolean IsNew { get; set; }
        public Int32 OrderSort { get; set; }

        public Int32 ID { get; set; }
        public Int32 Clint { get; set; }
        public DateTime Date { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime TrueDeliveryDate { get; set; }
        public Int32 Car { get; set; }
        public Int32 Driver { get; set; }
        public Double Weight { get; set; }
        public string InvoiceCode { get; set; }
        public Int32 OrderGroup { get; set; }
        public Double TotalPrice { get; set; }
        public Int32 Status { get; set; }
        public Boolean Reported { get; set; }
        public DataTable Details { get ; set; }
        public void Save()
        {
            if (IsNew == true) { OrderSort = Master.GetNewID("OrderSort", "Order"); Master.GetNewID("ID", "Order"); Master.SQlCmd("Insert Into [Order](ID)Values( " + ID + ") "); }
            string SaveSQltxt = " Update [Order] set "
             + " ID = '" + ID + "' "
             + ",OrderSort = '" + OrderSort + "' "
             + ",Clint = '" + Clint + "' "
             + ",Date = '" + Master.SqlDate(Date) + "' "
             + ",DeliveryDate = '" + Master.SqlDate(DeliveryDate) + "' "
             + ",TrueDeliveryDate = '" + Master.SqlDate(TrueDeliveryDate) + "' "
             + ",Car = '" + Car + "' "
             + ",Driver = '" + Driver + "' "
             + ",Weight = '" + Weight + "' "
             + ",InvoiceCode = '" + InvoiceCode + "' "
             + ",OrderGroup = '" + OrderGroup + "' "
             + ",TotalPrice = '" + TotalPrice + "' "
             + ",Status = '" + Status + "' "
             + ",Reported = '" + Reported + "' "
             + " from  [Order] where ID  = '" + ID + "' ";
            Master.SQlCmd(SaveSQltxt);
            IsNew = false;
            SaveDetails();
            SendRefresh();
            Master.insertNotfy(0, ID);

        }
        public static void SendRefresh()
        {
            foreach (Form frm in Master.MainForm.MdiChildren)
            {
                if(frm.Name.Length > 10 &&  frm.Name.Substring(0, 10) == "frm_Orders")
                {
                ((Forms.frm_Orders)frm).RefreshOrdersDT();
                }
            }
            Master.MainForm.CalculateNotficationPanal();
        }
         void ClearDetails() { Master.SQlCmd(" delete from OrderProducts where OrderID  = '" + ID + "'"); }
        private void SaveDetails()
        {
            ClearDetails();
            Details.AcceptChanges();
            foreach (DataRow  r in Details.Rows )
            {
                new OrderProduct(ID, (int)r["Product"], (double)r["Qty"], (double)r["Weight"], (double)r["TotalPrice"]).Save();
            }

        }
        public void Delete()
        {
         
            if (Master.ShowDeleteConfrmationDialog(Master.MainForm, "هل تريد حذف الطلبيه ؟") == true)
            {
                Master.SQlCmd(" delete from [Order] where ID  = '" + ID + "'");
                ClearDetails();
                IsNew = true;
                SendRefresh();
            }
        }


   

         void getDetails()
        {
           Details = Master.SqlDataTable("Select  Product ,Qty , Weight ,TotalPrice From [OrderProducts]  where OrderID  = '" + ID + "' ");  
           
        }
    public static DataTable ToData()
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From [Order]  ");
            return DA;
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp)
        {
            lookUp.Properties.DataSource = ToData();
            lookUp.Properties.ValueMember = "ID";
            lookUp.Properties.DisplayMember = "Clint";
        }

    }
}
