﻿using DevExpress.XtraGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwiftOrders.DAL
{
    public  class User
    {
       
        public void Delete()
        {
            if ((Int32)Master.SqlDataTable(" select COUNT(ID)FROM St_UserLog   where UserID = '" + ID + "'").Rows[0][0] != 0) { Master.ShowInformMessseg(Master.MainForm, LangResorces.CantDelete, LangResorces.DeleteAbortMesg); return; }
            if (Master.ShowDeleteConfrmationDialog(Master.MainForm, Master.ReplaceMsgCont(LangResorces.ConfrmDeleteUser, UserName)) == true)
            {
                Master.SQlCmd(" delete from St_User where ID  = '" + ID + "'");
                ClearPrivilages();
                IsNew = true;
                //Master.MainForm.SetStatuesBar(Master.ReplaceMsgCont(LangResorces.UserNameDeleteSucc, UserName), Color.Green);
            }
        }
        public static DataTable ToData()
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From St_User  ");
            return DA;
        }
        public static void ToGridView(GridControl gridControl )
        {
            
            gridControl .DataSource  = Master.SqlDataTable("Select ID, [UserName]  From St_User  ");
            
            
        }
        public DataTable GetPrivilaege()
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select PrivilageName,PrivilegeValue From St_UserPrivilege where UserID = '" + ID + "'  ");
            return DA;
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp)
        {
            lookUp.Properties.DataSource = ToData();
            lookUp.Properties.ValueMember = " ID ";
            lookUp.Properties.DisplayMember = " UserName ";
        }
        public static bool ValidateLogIn(string username, string password)
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select   ID, [UserName] ,[PassWord],HasAccess,AccessByTime,AccessByTimeType,FromTime,ToTime From St_User where UserName  = '" + username + "' and Password = '" + Cryptography.Encrypt(password) + "' ");
            if (DA.Rows.Count >= 1)
            {
                if((bool)DA.Rows[0][3] == true)
                {
                    if((bool)DA.Rows[0][4] == false)
                    {
                        CurrentUser.user = new User(Convert.ToInt32(DA.Rows[0][0]));
                        CurrentUser.user.PassWord = password;
                        CurrentUser.LoadPrivilage();
                        return true;
                    }
                    else
                    {
                        if ((bool)DA.Rows[0][5] == true )
                        {
                            string FromTime = ((DateTime)DA.Rows[0][6]).ToString("hh:mm:ss tt");
                            string ToTime = ((DateTime)DA.Rows[0][7]).ToString("hh:mm:ss tt");
                            DateTime sTime, endtime;
                            sTime = Convert.ToDateTime(DateTime.Now.ToString(" yyyy-MM-dd ") + FromTime);
                            endtime = Convert.ToDateTime(DateTime.Now.ToString(" yyyy-MM-dd ") + ToTime);
                            if(DateTime.Compare(sTime, DateTime.Now) == -1 && DateTime.Compare(endtime, DateTime.Now) == 1)
                            {
                                CurrentUser.user = new User(Convert.ToInt32(DA.Rows[0][0]));
                                CurrentUser.user.PassWord = password;
                                CurrentUser.LoadPrivilage();
                                return true;

                            }
                            else
                            {
                                MessageBox.Show("Sorry you cant login at this time ");
                                return false;
                            }

                        }
                        else
                        {
                            // ToDo After configrinng HR attedace
                            CurrentUser.user = new User(Convert.ToInt32(DA.Rows[0][0]));
                            CurrentUser.user.PassWord = password;
                            CurrentUser.LoadPrivilage();
                            return true;
                        }
                    }
                   
                }
                else
                {
                    MessageBox.Show("Sorry your account has been susspended");
                    return false;
                }
            }
            else
                return false;
        }
        public User()
        {
            IsNew = true;
            ID = Master.GetNewID("ID", "St_User");
        }
        public User(int id)
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From St_User where ID  = '" + id + "' ");
            IsNew = false;
            ID = (Int32)DA.Rows[0]["ID"];
            UserName = (string)DA.Rows[0]["UserName"];
            PassWord =Cryptography.Decrypt( (string)DA.Rows[0]["PassWord"]);
            HasAccess = (Boolean)DA.Rows[0]["HasAccess"];
            AccessByTime = (Boolean)DA.Rows[0]["AccessByTime"];
            AccessByTimeType = (Boolean)DA.Rows[0]["AccessByTimeType"];

            if (AccessByTime)
            {
                if (AccessByTimeType)
                {
                    FromTime = (DateTime)DA.Rows[0]["FromTime"];
                    ToTime = (DateTime)DA.Rows[0]["ToTime"];
                }
                else
                {
                    FromTime = DateTime.Now;
                    ToTime = DateTime.Now;
                }
            }
            AccountSecrecyLevel = (Int32)DA.Rows[0]["AccountSecrecyLevel"];
            CustomersGroup = (Int32)DA.Rows[0]["CustomersGroup"];
            DefaultCustomer = (Int32)DA.Rows[0]["DefaultCustomer"];
            CanChangeStore = (Boolean)DA.Rows[0]["CanChangeStore"];
            CanChangeDrawer = (Boolean)DA.Rows[0]["CanChangeDrawer"];
            CanChangeCCenter = (Boolean)DA.Rows[0]["CanChangeCCenter"];
            DefaultStore = (int)DA.Rows[0]["DefaultStore"];
            DefaultDrawer = (int)DA.Rows[0]["DefaultDrawer"];
            DefaultRawStore = (int)DA.Rows[0]["DefaultRawStore"];
            ShowItemBalance = (Boolean)DA.Rows[0]["ShowItemBalance"];
            SellLowerPriceThanBuy = (Boolean)DA.Rows[0]["SellLowerPriceThanBuy"];
            WhenSellingQtyLessThanReorder = (Int32)DA.Rows[0]["WhenSellingQtyLessThanReorder"];
            WhenSellingQtyNoBalance = (Int32)DA.Rows[0]["WhenSellingQtyNoBalance"];
            WhenSellingToMaxCreditCustomer = (Int32)DA.Rows[0]["WhenSellingToMaxCreditCustomer"];
            HideItemDiscount = (Boolean)DA.Rows[0]["HideItemDiscount"];
            HideItemCost = (Boolean)DA.Rows[0]["HideItemCost"];
            CanChangeItemPrice = (Boolean)DA.Rows[0]["CanChangeItemPrice"];
            CanAddTotalDiscount = (Boolean)DA.Rows[0]["CanAddTotalDiscount"];
            CanSellByCredit = (Boolean)DA.Rows[0]["CanSellByCredit"];
            WhenAddingQtyMaxBalance = (Int32)DA.Rows[0]["WhenAddingQtyMaxBalance"];
            CanChangeCashNotesDate = (Boolean)DA.Rows[0]["CanChangeCashNotesDate"];
            CanEditInClosedPeriod = (Boolean)DA.Rows[0]["CanEditInClosedPeriod"];
            PrintAfterSaving = (Boolean)DA.Rows[0]["PrintAfterSaving"];
        }
        public Boolean IsNew { get; set; }
        public Int32 ID { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public Boolean HasAccess { get; set; }
        public Boolean AccessByTime { get; set; }
        public Boolean AccessByTimeType { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public Int32 AccountSecrecyLevel { get; set; }
        public Int32 DefaultCustomer { get; set; }

        public Int32 CustomersGroup { get; set; }
        public Boolean CanChangeStore { get; set; }
        public Boolean CanChangeDrawer { get; set; }
        public Boolean CanChangeCCenter { get; set; }
        public int DefaultStore { get; set; }
        public int DefaultDrawer { get; set; }
        public int DefaultRawStore { get; set; }
        public Boolean ShowItemBalance { get; set; }
        public Boolean SellLowerPriceThanBuy { get; set; }
        public Int32 WhenSellingQtyLessThanReorder { get; set; }
        public Int32 WhenSellingQtyNoBalance { get; set; }
        public Int32 WhenSellingToMaxCreditCustomer { get; set; }
        public Boolean HideItemDiscount { get; set; }
        public Boolean HideItemCost { get; set; }
        public Boolean CanChangeItemPrice { get; set; }
        public Boolean CanAddTotalDiscount { get; set; }
        public Boolean CanSellByCredit { get; set; }
        public Int32 WhenAddingQtyMaxBalance { get; set; }
        public Boolean CanChangeCashNotesDate { get; set; }
        public Boolean CanEditInClosedPeriod { get; set; }
        public Boolean PrintAfterSaving { get; set; }
        public void Save()
        {
            if (IsNew == true) { Master.GetNewID("ID", "St_User"); Master.SQlCmd("Insert Into St_User(ID)Values( " + ID + ") "); }
            string SaveSQltxt = " Update St_User set "
             + " ID = '" + ID + "' "
             + ",UserName = '" + UserName + "' "
             + ",PassWord = '" + Cryptography.Encrypt(PassWord) + "' "
             + ",HasAccess = '" + HasAccess + "' "
             + ",AccessByTime = '" + AccessByTime + "' "
             + ",AccessByTimeType = '" + AccessByTimeType + "' "
             + ",FromTime = '" + Master.SqlDate(FromTime) + "' "
             + ",ToTime = '" + Master.SqlDate(ToTime) + "' "
             + ",AccountSecrecyLevel = '" + AccountSecrecyLevel + "' "
             + ",CustomersGroup = '" + CustomersGroup + "' "
             + ",DefaultCustomer = '" + DefaultCustomer + "' "
             + ",CanChangeStore = '" + CanChangeStore + "' "
             + ",CanChangeDrawer = '" + CanChangeDrawer + "' "
             + ",CanChangeCCenter = '" + CanChangeCCenter + "' "
             + ",DefaultStore = '" + DefaultStore + "' "
             + ",DefaultDrawer = '" + DefaultDrawer + "' "
             + ",DefaultRawStore = '" + DefaultRawStore + "' "
             + ",ShowItemBalance = '" + ShowItemBalance + "' "
             + ",SellLowerPriceThanBuy = '" + SellLowerPriceThanBuy + "' "
             + ",WhenSellingQtyLessThanReorder = '" + WhenSellingQtyLessThanReorder + "' "
             + ",WhenSellingQtyNoBalance = '" + WhenSellingQtyNoBalance + "' "
             + ",WhenSellingToMaxCreditCustomer = '" + WhenSellingToMaxCreditCustomer + "' "
             + ",HideItemDiscount = '" + HideItemDiscount + "' "
             + ",HideItemCost = '" + HideItemCost + "' "
             + ",CanChangeItemPrice = '" + CanChangeItemPrice + "' "
             + ",CanAddTotalDiscount = '" + CanAddTotalDiscount + "' "
             + ",CanSellByCredit = '" + CanSellByCredit + "' "
             + ",WhenAddingQtyMaxBalance = '" + WhenAddingQtyMaxBalance + "' "
             + ",CanChangeCashNotesDate = '" + CanChangeCashNotesDate + "' "
             + ",CanEditInClosedPeriod = '" + CanEditInClosedPeriod + "' "
             + ",PrintAfterSaving = '" + PrintAfterSaving + "' "
             + " from  St_User where ID  = '" + ID + "' ";
            Master.SQlCmd(SaveSQltxt);
            IsNew = false;
        }
        public void ClearPrivilages()
        {
            Master.SQlCmd("delete from St_UserPrivilege Where UserID = '"+ ID +"'");
        }
     

    }
}
