﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftOrders.DAL
{
    class Shipment
    {
        public Shipment()
        {
            IsNew = true;
            ID = Master.GetNewID("ID", "Shipment");
            Date = DateTime.Now;

        }
        public Shipment(int id)
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Shipment where ID  = '" + id + "' ");
            IsNew = false;
            ID = (Int32)DA.Rows[0]["ID"];
            Date = (DateTime)DA.Rows[0]["Date"];
            Start = (Int32)DA.Rows[0]["Start"];
            Car = (Int32)DA.Rows[0]["Car"];
            Driver = (Int32)DA.Rows[0]["Driver"];
            Measurement_ODO = (Int32)DA.Rows[0]["Measurement_ODO"];
            Measurement_Fuel = (Int32)DA.Rows[0]["Measurement_Fuel"];
        }
        public Boolean IsNew { get; set; }
        public Int32 ID { get; set; }
        public DateTime Date { get; set; }
        public Int32 Start { get; set; }
        public Int32 Car { get; set; }
        public Int32 Driver { get; set; }
        public Int32 Measurement_ODO { get; set; }
        public Int32 Measurement_Fuel { get; set; }
        public void Save()
        {
            if (IsNew == true) { Master.GetNewID("ID", "Shipment"); Master.SQlCmd("Insert Into Shipment(ID)Values( " + ID + ") "); }
            string SaveSQltxt = " Update Shipment set "
             + " ID = '" + ID + "' "
             + ",Date = '" + Master.SqlDate(Date) + "' "
             + ",Start = '" + Start + "' "
             + ",Car = '" + Car + "' "
             + ",Driver = '" + Driver + "' "
             + ",Measurement_ODO = '" + Measurement_ODO + "' "
             + ",Measurement_Fuel = '" + Measurement_Fuel + "' "
             + " from  Shipment where ID  = '" + ID + "' ";
            Master.SQlCmd(SaveSQltxt);
          

            IsNew = false;
        }
        public void Delete()
        {
            //if ((Int32)Master.SqlDataTable(" select COUNT( )FROM   where ID = '" + ID + "'").Rows[0][0] != 0) { Master.ShowInformMessseg(Master.MainForm, LangResorces.CantDelete, LangResorces.DeleteAbortMesg); return; }
            //if (Master.ShowDeleteConfrmationDialog(Master.MainForm, LangResorces.ConfrmDeleteStore + Name) == true)
            //{
            //    Master.SQlCmd(" delete from Shipment where ID  = '" + ID + "'");
            //    IsNew = true;
            //    Master.MainForm.SetStatuesBar(Master.ReplaceMsgCont(LangResorces.DateDeleteSucc, Date), Color.Green);
            //}
        }
        public static DataTable ToData()
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Shipment  ");
            return DA;
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp)
        {
            lookUp.Properties.DataSource = ToData();
            lookUp.Properties.ValueMember = " ID ";
            lookUp.Properties.DisplayMember = " Date ";
        }

    }
}
