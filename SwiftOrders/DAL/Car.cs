﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftOrders.DAL
{
    class Car
    {
        public Car()
        {
            IsNew = true;
            ID = Master.GetNewID("ID", "Car");
        }
        public Car(int id)
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From Car where ID  = '" + id + "' ");
            IsNew = false;
            ID = (Int32)DA.Rows[0]["ID"];
            Driver = Convert.ToInt32 ( DA.Rows[0]["Driver"]);
            PlateNB = (string)DA.Rows[0]["PlateNB"];
            CargoCap = (Double)DA.Rows[0]["CargoCap"];
            TankCap = (Double)DA.Rows[0]["TankCap"];
            CarColor = (string)DA.Rows[0]["CarColor"];
            EngSerial = (string)DA.Rows[0]["EngSerial"];
            ChassisNumber = (string)DA.Rows[0]["ChassisNumber"];
            ModelYear = (DateTime)DA.Rows[0]["ModelYear"];
            ModelNumber = (string)DA.Rows[0]["ModelNumber"];
            CurrentRegstrationStartDate = (DateTime)DA.Rows[0]["CurrentRegstrationStartDate"];
            CurrentRegstrationEndDate = (DateTime)DA.Rows[0]["CurrentRegstrationEndDate"];
            TireSize = (string)DA.Rows[0]["TireSize"];
        }
        public Boolean IsNew { get; set; }
        public Int32 ID { get; set; }
        public Int32 Driver { get; set; }
        public string PlateNB { get; set; }
        public Double CargoCap { get; set; }
        public Double TankCap { get; set; }
        public string CarColor { get; set; }
        public string EngSerial { get; set; }
        public string ChassisNumber { get; set; }
        public DateTime ModelYear { get; set; }
        public string ModelNumber { get; set; }
        public DateTime CurrentRegstrationStartDate { get; set; }
        public DateTime CurrentRegstrationEndDate { get; set; }
        public string TireSize { get; set; }
        public void Save()
        {
            if (IsNew == true) { Master.GetNewID("ID", "Car"); Master.SQlCmd("Insert Into Car(ID)Values( " + ID + ") "); }
            string SaveSQltxt = " Update Car set "
             + " ID = '" + ID + "' "
             + ",Driver = '" + Driver  + "' "
             + ",PlateNB = '" + PlateNB + "' "
             + ",CargoCap = '" + CargoCap + "' "
             + ",TankCap = '" + TankCap + "' "
             + ",CarColor = '" + CarColor + "' "
             + ",EngSerial = '" + EngSerial + "' "
             + ",ChassisNumber = '" + ChassisNumber + "' "
             + ",ModelYear = '" + Master.SqlDate(ModelYear) + "' "
             + ",ModelNumber = '" + ModelNumber + "' "
             + ",CurrentRegstrationStartDate = '" + Master.SqlDate(CurrentRegstrationStartDate) + "' "
             + ",CurrentRegstrationEndDate = '" + Master.SqlDate(CurrentRegstrationEndDate) + "' "
             + ",TireSize = '" + TireSize + "' "
             + " from  Car where ID  = '" + ID + "' ";
            Master.SQlCmd(SaveSQltxt);
            IsNew = false;
        }
        public void Delete()
        {
            //if ((Int32)Master.SqlDataTable(" select COUNT(ID)FROM  [Order] where Car = '" + ID + "'").Rows[0][0] != 0) { Master.ShowInformMessseg(Master.MainForm, LangResorces.CantDelete, LangResorces.DeleteAbortMesg); return; }
            if (Master.ShowDeleteConfrmationDialog(Master.MainForm," هل تريد حذف السياره ؟") == true)
            {
                Master.SQlCmd(" delete from Car where ID  = '" + ID + "'");
                IsNew = true; 

            }
         
        }
        public static DataTable ToData(bool ShowAllColumns = true )
        {
            DataTable DA = new DataTable();
            if (ShowAllColumns)
            {
                DA = Master.SqlDataTable("Select * From Car  ");

            }
            else
            {
                DA = Master.SqlDataTable("Select ID,PlateNB,CarColor From Car  ");

            }

            return DA;
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp)
        {
            lookUp.Properties.DataSource = ToData(false);
            lookUp.Properties.ValueMember = "ID";
            //lookUp.Properties.Columns["ID"].Visible = false;
            lookUp.Properties.DisplayMember = "PlateNB";
        }

    }
}


