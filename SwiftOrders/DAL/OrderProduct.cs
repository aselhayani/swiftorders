﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftOrders.DAL
{
    class OrderProduct
    {
        public OrderProduct(int orderid, int product, double qty, double weight, double totalprice = 0)
        {
            IsNew = true;
            ID = Master.GetNewID("ID", "OrderProducts");
            OrderID = orderid;
            Product = product;
            Qty = qty;
            Weight = weight;
            TotalPrice = totalprice;


        }
    
        public OrderProduct(int id)
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From OrderProducts where ID  = '" + id + "' ");
            IsNew = false;
            ID = (Int32)DA.Rows[0]["ID"];
            OrderID = (Int32)DA.Rows[0]["OrderID"];
            Product = (Int32)DA.Rows[0]["Product"];
            Qty = (Double)DA.Rows[0]["Qty"];
            Weight = (Double)DA.Rows[0]["Weight"];
            TrueQty = (Double)DA.Rows[0]["TrueQty"];
            TrueWeight = (Double)DA.Rows[0]["TrueWeight"];
            TotalPrice = (Double)DA.Rows[0]["TotalPrice"];
        }
        public Boolean IsNew { get; set; }
        public Int32 ID { get; set; }
        public Int32 OrderID { get; set; }
        public Int32 Product { get; set; }
        public Double Qty { get; set; }
        public Double Weight { get; set; }
        public Double TrueQty { get; set; }
        public Double TrueWeight { get; set; }
        public Double TotalPrice { get; set; }
        public void Save()
        {
            if (IsNew == true) { Master.GetNewID("ID", "OrderProducts"); Master.SQlCmd("Insert Into OrderProducts(ID)Values( " + ID + ") "); }
            string SaveSQltxt = " Update OrderProducts set "
             + " ID = '" + ID + "' "
             + ",OrderID = '" + OrderID + "' "
             + ",Product = '" + Product + "' "
             + ",Qty = '" + Qty + "' "
             + ",Weight = '" + Weight + "' "
             + ",TrueQty = '" + TrueQty + "' "
             + ",TrueWeight = '" + TrueWeight + "' "
             + ",TotalPrice = '" + TotalPrice + "' "
             + " from  OrderProducts where ID  = '" + ID + "' ";
            Master.SQlCmd(SaveSQltxt);
            IsNew = false;
        }
        public void Delete()
        {
            //if ((Int32)Master.SqlDataTable(" select COUNT( )FROM   where ID = '" + ID + "'").Rows[0][0] != 0) { Master.ShowInformMessseg(Master.MainForm, LangResorces.CantDelete, LangResorces.DeleteAbortMesg); return; }
            //if (Master.ShowDeleteConfrmationDialog(Master.MainForm, LangResorces.ConfrmDeleteStore + Name) == true)
            //{
            //    Master.SQlCmd(" delete from OrderProducts where ID  = '" + ID + "'");
            //    IsNew = true;
            //    //Master.MainForm.SetStatuesBar(Master.ReplaceMsgCont(LangResorces.OrderIDDeleteSucc, OrderID), Color.Green);
            //}
        }
        public static DataTable ToData()
        {
            DataTable DA = new DataTable();
            DA = Master.SqlDataTable("Select * From OrderProducts  ");
            return DA;
        }
        public static void ToLookUp(DevExpress.XtraEditors.LookUpEdit lookUp)
        {
            lookUp.Properties.DataSource = ToData();
            lookUp.Properties.ValueMember = " ID ";
            lookUp.Properties.DisplayMember = " OrderID ";
        }


    }
}
